import 'dart:async';
import 'dart:io';
import 'dart:isolate';

import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:driver_share/app/values/themes/app_themes.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:intl/intl.dart';

import 'app/data/remote_db/models/location_models/get_location_response.dart';
import 'app/data/remote_db/models/location_models/location_response.dart';
import 'app/data/remote_db/provider/location_provider.dart';
import 'app/modules/global_components/bindings/global_components_binding.dart';
import 'app/modules/global_components/controllers/loading_controller.dart';
import 'app/modules/global_components/controllers/location_controller.dart';
import 'app/routes/app_pages.dart';
import 'app/values/utilities/storage_pref.dart';

void main() async {
  await GetStorage.init();

  HttpOverrides.global = MyHttpOverrides();
  awesomeNotification();
  runApp(const MyApp());
}




class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  var initialRoute = Routes.LOGIN;
  @override
  void initState() {
    super.initState();
    route();
  }
  var storage = Get.put(StoragePref());
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: "Driver Share",
      // initialRoute: storage.isLoggedIn.val ? Routes.DRIVER_HOME : Routes.LOGIN,
      initialRoute: initialRoute,
      // initialRoute: Routes.PROVIDER_HOME,
      getPages: AppPages.routes,
      theme: Themes.lightTheme,
      darkTheme: Themes.lightTheme,
      themeMode: ThemeMode.system,
      initialBinding: GlobalComponentsBinding(),
    );
  }

  void route(){
    setState(() {
      if(storage.isLoggedIn.val ){
        if(storage.isDriverLoggedIn.val){
          initialRoute= Routes.DRIVER_HOME;
        }else {
          initialRoute= Routes.PROVIDER_HOME;
        }
      }else{
        initialRoute= Routes.LOGIN;
      }
    });
  }
}

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}


Future<void> awesomeNotification()async{
  await AwesomeNotifications().initialize(
      // 'resource://mipmap-hdpi/ic_launcher',
    null,
      [
        NotificationChannel(
          channelKey: 'basic_channel',
          channelName: 'Basic Notification',
          channelGroupKey: 'basic_channel_group',
          defaultColor: onPrimary,
          importance: NotificationImportance.High,
          channelShowBadge: true,
          playSound: true,
          enableVibration: true,
          channelDescription: "Basic driver_notification description"
        ),
        NotificationChannel(
          channelKey: 'call_channel',
          channelName: 'Call Notification',
          channelGroupKey: 'call_channel_group',
          defaultColor: onPrimary,
          importance: NotificationImportance.High,
          channelShowBadge: true,
          playSound: true,
          enableVibration: true,
          channelDescription: "Call driver_notification"
        ),
      ],
      channelGroups: [
        NotificationChannelGroup(
            channelGroupkey: 'basic_channel_group',
            channelGroupName: 'Basic group'),
        NotificationChannelGroup(
            channelGroupkey: 'call_channel_group',
            channelGroupName: 'Call group'),
      ],
      debug: true
  );
}