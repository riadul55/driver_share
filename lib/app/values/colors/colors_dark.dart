import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ColorsDark {
  static var primary = Color.fromRGBO(34, 39, 50, 1);
  static var onPrimary = Color(0xFFE5E5E5);
  static var primaryVariant = Color(0xffe3161a);

  static var secondary = Color.fromRGBO(42, 48, 60, 1);
  static var onSecondary = Color(0xFFE5E5E5);
  static var secondaryVariant = Color.fromRGBO(134, 141, 154, 1);

  static var background = Color.fromRGBO(25, 29, 36, 1);
  static var onBackground = Color.fromRGBO(255, 252, 252, 1);

  static var error = Colors.red;
  static var onError = Colors.white;

  static var surface = Color(0xFFE5E5E5);
  static var onSurface = Color(0xFFE5E5E5);

  static var primaryText = Colors.white;
  static var secondaryText = Color(0xFFFFFDFD);
}