import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ColorsLight {
  static var primary = Color(0xFFE5E5E5);
  static var onPrimary = Color(0xFF655DB0);
  static var primaryVariant = Color(0xFF655DB0);

  static var secondary = Color(0xFFE5E5E5);
  static var onSecondary = Color.fromRGBO(42, 48, 60, 1);
  static var secondaryVariant = Color.fromRGBO(134, 141, 154, 1);

  static var background = Color(0xFFF8F8F8);
  static var onBackground = Color.fromRGBO(25, 29, 36, 1);

  static var error = Colors.red;
  static var onError = Colors.white;

  static var surface = Colors.white;
  static var onSurface = Color(0xFFE5E5E5);

  static var primaryText = Colors.black;
  static var secondaryText = Colors.grey;
}
