import 'package:driver_share/app/values/utilities/storage_pref.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:intl/intl.dart';

class Constants {
  static final APP_VERSION = "2.0.1";
  static final defaultPadding = 20.0;

  static final xsText = 12.0;
  static final sText = 14.0;
  static final mText = 16.0;
  static final lText = 18.0;
  static final xlText = 20.0;

  static final INDEX_HOME = 0;
  static final INDEX_CAMPAIGN = 1;
  static final INDEX_CART = 2;
  static final INDEX_PROFILE = 3;

  static final CURRENCY_ICON = "৳";


  static final ROOT_URL = "https://labapi.yuma-technology.co.uk:9022";
  static final BASE_URL = "$ROOT_URL/driver_app";

  static final POSTCODE_ROOT_URL="https://labapi.yuma-technology.co.uk:8443/delivery";



  StoragePref storage = Get.find<StoragePref>();
  static headers({sessionId}) =>
      {
        "DriverSession": "$sessionId",
        // "app-key": "$APP_KEY",
      };

  static int getTimestamp(String startTime) {
    var now = DateTime.now();
    var format = DateFormat("yyyy-MM-dd hh:mm a");
    var startDateTime = format.parse(startTime, true);
    // var diff = now.difference(startDateTime);
    return startDateTime.millisecondsSinceEpoch;
  }
}


class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}
