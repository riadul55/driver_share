import 'package:driver_share/app/values/utilities/get_storage_key.dart';
import 'package:get_storage/get_storage.dart';

class StoragePref {
  static final box = () => GetStorage();

  var isLoggedIn = false.val(GetStorageKey.IS_LOGGED_IN, getBox: box);
  var isDriverLoggedIn = false.val(GetStorageKey.IS_DRIVER_LOGGED_IN, getBox: box);
  var sessionId = "".val(GetStorageKey.SESSION_ID, getBox: box);
  var driverUuid = "".val(GetStorageKey.DRIVER_UUID, getBox: box);
  var providerUuid = "".val(GetStorageKey.PROVIDER_UUID, getBox: box);
  var defaultRadius="2000".val(GetStorageKey.DEFAULT_RADIUS, getBox: box);
  var loginID="".val(GetStorageKey.LOGIN_ID);
  var interval = 0.val("interval", getBox: box);
}