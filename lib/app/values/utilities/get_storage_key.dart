class GetStorageKey {
  static const IS_DARK_MODE = "isDarkTheme";
  static const IS_FIRST_RUN = "isFirstRun";
  static final IS_LOGGED_IN = "isLoggedIn";
  static final IS_DRIVER_LOGGED_IN = "isDriverLoggedIn";
  static const USER_LOGIN_INFO = "userLoginInfo";
  static const USER_INFO = "userInfo";
  static const SESSION_ID = "sessionId";
  static const DRIVER_UUID = "driverUuid";
  static const PROVIDER_UUID = "providerUuid";
  static const DEFAULT_RADIUS = "defaultRadius";
  static const LOGIN_ID = "loginId";
}