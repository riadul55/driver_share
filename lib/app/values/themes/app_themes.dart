import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

const onPrimary = Color(0xFF655DB0);
const primaryContainer = Color(0xFF655DB0);
const primaryVariant = Color(0xFF655DB0);
const pastelGreen = Color(0xFFAEDA9D);
const pastelPurple = Color(0xFFB4A4FF);
const pastelRed = Color(0xFFF9CEDF);
const pastelAsh = Color(0xFFD7DDE9);
const pastelBlue = Color(0xFF8CBBF1);
const pastelYellow = Color(0xFFFCEECB);
class Themes{
  static final lightTheme = ThemeData(
    fontFamily: GoogleFonts.openSans().fontFamily,
    textButtonTheme: TextButtonThemeData(
      style: TextButton.styleFrom(
        primary: Colors.black
      ),
    ),
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(
        primary: primaryContainer,
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      ),
    ),
    colorScheme: const ColorScheme.light(
      // primary: onPrimary,
      primary: Colors.white,
      onPrimary: Color.fromRGBO(34, 39, 50, 1),

      secondary: Color(0xFFE5E5E5),
      onSecondary: Color.fromRGBO(42, 48, 60, 1),

      background: Colors.white,
      onBackground: Colors.black,

      primaryContainer: primaryContainer,
      secondaryContainer:  Color.fromRGBO(134, 141, 154, 1),

      surface: Colors.white,
      onSurface: Color(0xFFE5E5E5),

      error: Colors.red,
      onError: Colors.white,
      brightness: Brightness.light,
    ),
  );



  static final darkTheme = ThemeData(
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(
        primary: primaryContainer,
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      ),
    ),

    colorScheme:  const ColorScheme.dark(
      primary: Color.fromRGBO(34, 39, 50, 1),
      onPrimary: Colors.white,

      secondary: Color.fromRGBO(42, 48, 60, 1),
      onSecondary:  Color(0xFFE5E5E5),

      background: Colors.black,
      onBackground: Colors.white,

      primaryContainer: primaryContainer,
      secondaryContainer:  Color.fromRGBO(134, 141, 154, 1),

      surface: Color(0xFFE5E5E5),
      onSurface: Colors.white,

      error: Colors.red,
      onError: Colors.white,
      brightness: Brightness.dark,
    ),
  );


}