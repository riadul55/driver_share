class ProviderLoginResponse {
  ProviderSuccessResponse? providerSuccessResponse;
  ProviderErrorResponse? providerErrorResponse;

  ProviderLoginResponse(
      {this.providerSuccessResponse, this.providerErrorResponse});

  ProviderLoginResponse.formJson(Map<String, dynamic> json)
      : providerSuccessResponse = ProviderSuccessResponse.fromJson(json),
        providerErrorResponse = null;

  ProviderLoginResponse.withError(Map<String, dynamic> json)
      : providerSuccessResponse = null,
        providerErrorResponse = ProviderErrorResponse.fromJson(json);
}

class ProviderSuccessResponse {
  String? session_id;
  String? provider_uuid;

  ProviderSuccessResponse({
    this.session_id,
    this.provider_uuid,
  });

  ProviderSuccessResponse.fromJson(Map<String, dynamic> json) {
    session_id = json['session_id']?.toString();
    provider_uuid = json['provider_uuid']?.toString();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['session_id'] = session_id;
    data['provider_uuid'] = provider_uuid;
    return data;
  }
}

class ProviderErrorResponse {
  String? error_code;
  String? error_description;

  ProviderErrorResponse({
    this.error_code,
    this.error_description,
  });

  ProviderErrorResponse.fromJson(Map<String, dynamic> json) {
    error_code = json['error_code']?.toString();
    error_description = json['error_description']?.toString();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['error_code'] = error_code;
    data['error_description'] = error_description;
    return data;
  }
}
