class ProviderSignUpResponse {
  bool success;
  String message;

  ProviderSignUpResponse(this.success, this.message);
}