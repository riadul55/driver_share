class DriverSignUpResponse {
  bool success;
  String message;

  DriverSignUpResponse(this.success, this.message);
}