class DriverLoginResponse {
  LoginSuccessResponse? loginSuccessResponse;
  DriverErrorResponse? driverErrorResponse;

  DriverLoginResponse(
      {this.loginSuccessResponse, this.driverErrorResponse});

  DriverLoginResponse.formJson(Map<String, dynamic> json)
      : loginSuccessResponse = LoginSuccessResponse.fromJson(json),
        driverErrorResponse = null;

  DriverLoginResponse.withError(Map<String, dynamic> json)
      : loginSuccessResponse = null,
        driverErrorResponse = DriverErrorResponse.fromJson(json);
}

class LoginSuccessResponse {
  String? session_id;
  String? driver_uuid;

  LoginSuccessResponse({
    this.session_id,
    this.driver_uuid,
  });
  LoginSuccessResponse.fromJson(Map<String, dynamic> json) {
    session_id = json['session_id']?.toString();
    driver_uuid = json['driver_uuid']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['session_id'] = session_id;
    data['driver_uuid'] = driver_uuid;
    return data;
  }
}

class DriverErrorResponse {
  String? error_code;
  String? error_description;

  DriverErrorResponse({
    this.error_code,
    this.error_description,
  });
  DriverErrorResponse.fromJson(Map<String, dynamic> json) {
    error_code = json['error_code']?.toString();
    error_description = json['error_description']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['error_code'] = error_code;
    data['error_description'] = error_description;
    return data;
  }
}

