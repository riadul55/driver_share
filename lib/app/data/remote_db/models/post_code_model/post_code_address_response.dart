class PostCodeAddressResponse {
  List<AddressSuccessResponse>? addressSuccessResponse;
  AddressErrorResponse? addressErrorResponse;

  PostCodeAddressResponse(
      {this.addressSuccessResponse, this.addressErrorResponse});

  PostCodeAddressResponse.fromJson(List<dynamic> json)
      : addressSuccessResponse = (json).map((i) => AddressSuccessResponse.fromJson(i)).toList(),
        addressErrorResponse = null;

  PostCodeAddressResponse.withError(Map<String, dynamic> json)
      : addressSuccessResponse = [],
        addressErrorResponse = AddressErrorResponse();
}



class AddressSuccessResponse {
/*
{
  "postcode": "LU3 1EH",
  "town": "LUTON",
  "locality": "",
  "doubleLocality": "",
  "thoroughfare": "Argyll Avenue",
  "dependentThoroughfare": "",
  "buildingNumber": "100",
  "buildingName": "",
  "subBuildingName": "",
  "poBox": "",
  "departmentName": "",
  "organisationName": "",
  "udprn": "14216171",
  "type": "S",
  "suOrgIndicator": " ",
  "deliveryPointSuffix": "1A"
}
*/

  String? postcode;
  String? town;
  String? locality;
  String? doubleLocality;
  String? thoroughfare;
  String? dependentThoroughfare;
  String? buildingNumber;
  String? buildingName;
  String? subBuildingName;
  String? poBox;
  String? departmentName;
  String? organisationName;
  String? udprn;
  String? type;
  String? suOrgIndicator;
  String? deliveryPointSuffix;

  AddressSuccessResponse({
    this.postcode,
    this.town,
    this.locality,
    this.doubleLocality,
    this.thoroughfare,
    this.dependentThoroughfare,
    this.buildingNumber,
    this.buildingName,
    this.subBuildingName,
    this.poBox,
    this.departmentName,
    this.organisationName,
    this.udprn,
    this.type,
    this.suOrgIndicator,
    this.deliveryPointSuffix,
  });
  AddressSuccessResponse.fromJson(Map<String, dynamic> json) {
    postcode = json['postcode']?.toString();
    town = json['town']?.toString();
    locality = json['locality']?.toString();
    doubleLocality = json['doubleLocality']?.toString();
    thoroughfare = json['thoroughfare']?.toString();
    dependentThoroughfare = json['dependentThoroughfare']?.toString();
    buildingNumber = json['buildingNumber']?.toString();
    buildingName = json['buildingName']?.toString();
    subBuildingName = json['subBuildingName']?.toString();
    poBox = json['poBox']?.toString();
    departmentName = json['departmentName']?.toString();
    organisationName = json['organisationName']?.toString();
    udprn = json['udprn']?.toString();
    type = json['type']?.toString();
    suOrgIndicator = json['suOrgIndicator']?.toString();
    deliveryPointSuffix = json['deliveryPointSuffix']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['postcode'] = postcode;
    data['town'] = town;
    data['locality'] = locality;
    data['doubleLocality'] = doubleLocality;
    data['thoroughfare'] = thoroughfare;
    data['dependentThoroughfare'] = dependentThoroughfare;
    data['buildingNumber'] = buildingNumber;
    data['buildingName'] = buildingName;
    data['subBuildingName'] = subBuildingName;
    data['poBox'] = poBox;
    data['departmentName'] = departmentName;
    data['organisationName'] = organisationName;
    data['udprn'] = udprn;
    data['type'] = type;
    data['suOrgIndicator'] = suOrgIndicator;
    data['deliveryPointSuffix'] = deliveryPointSuffix;
    return data;
  }
}

class AddressErrorResponse{
  String? message;
  AddressErrorResponse({
    this.message,
  });
}