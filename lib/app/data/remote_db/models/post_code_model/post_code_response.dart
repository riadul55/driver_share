
class PostCodeResponse {
/*
"L1 0AA"
*/

  final List<String> successResponse;
  PostCodeErrorsResponse errorsResponse;



  PostCodeResponse.fromJson(List<dynamic> json)
    : successResponse = (json).map((i) => i.toString()).toList(),
        errorsResponse=PostCodeErrorsResponse();

  PostCodeResponse.withError(Map<String, dynamic>json)
  : successResponse=[],
  errorsResponse=PostCodeErrorsResponse.fromJson(json);


}



class PostCodeErrorsResponse {
  String? message;
  PostCodeErrorsResponse({
    this.message,
  });
  PostCodeErrorsResponse.fromJson(Map<String, dynamic> json) {
    message = json['message']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['message'] = message;
    return data;
  }

}