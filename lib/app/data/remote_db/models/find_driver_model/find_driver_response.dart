class FindDriverResponse {
  List<FindSuccessResponse>? findSuccessResponse;
  FindErrorResponse? findErrorResponse;

  FindDriverResponse(
      {this.findSuccessResponse, this.findErrorResponse});

  FindDriverResponse.fromJson(List<dynamic> json)
      : findSuccessResponse = (json).map((i) => FindSuccessResponse.fromJson(i)).toList(),
        findErrorResponse = null;

  FindDriverResponse.withError(Map<String, dynamic> json)
      : findSuccessResponse = [],
        findErrorResponse = FindErrorResponse.fromJson(json);
}


class FindSuccessResponse {
/*200
{
  "longitude": 90.43362667,
  "latitude": 23.76188,
  "timestamp": "2022-04-05 15:03:00",
  "heading": 66,
  "speed": 10,
  "driver_uuid": "00beb01c-1634-40d3-804c-374afb14eb13"
} */

  double? longitude;
  double? latitude;
  String? timestamp;
  double? heading;
  double? speed;
  String? driverUuid;

  FindSuccessResponse({
    this.longitude,
    this.latitude,
    this.timestamp,
    this.heading,
    this.speed,
    this.driverUuid,
  });
  FindSuccessResponse.fromJson(Map<String, dynamic> json) {
    longitude = json['longitude']?.toDouble();
    latitude = json['latitude']?.toDouble();
    timestamp = json['timestamp']?.toString();
    heading = json['heading']?.toDouble();
    speed = json['speed']?.toDouble();
    driverUuid = json['driver_uuid']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['longitude'] = longitude;
    data['latitude'] = latitude;
    data['timestamp'] = timestamp;
    data['heading'] = heading;
    data['speed'] = speed;
    data['driver_uuid'] = driverUuid;
    return data;
  }
}




class FindErrorResponse {
/*403
{
  "error_code": "SEC-0001",
  "error_description": "Invalid ProviderSession"
}
*/

  String? errorCode;
  String? errorDescription;

  FindErrorResponse({
    this.errorCode,
    this.errorDescription,
  });
  FindErrorResponse.fromJson(Map<String, dynamic> json) {
    errorCode = json['error_code']?.toString();
    errorDescription = json['error_description']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['error_code'] = errorCode;
    data['error_description'] = errorDescription;
    return data;
  }
}

