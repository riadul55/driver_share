class OrderNotificationResponse {
  bool success;
  String message;

  OrderNotificationResponse(this.success, this.message);
}


class OrderNotification {
/*
{
  "message": "success"
}
*/

  String? message;

  OrderNotification({
    this.message,
  });
  OrderNotification.fromJson(Map<String, dynamic> json) {
    message = json['message']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['message'] = message;
    return data;
  }
}
