class Position {
  double? latitude;
  double? longitude;
  double? heading;

  Position({this.latitude, this.longitude,this.heading});
}