import 'package:driver_share/app/data/remote_db/models/auth_models/driver_login_response.dart';
import 'package:driver_share/app/data/remote_db/models/location_models/location_response.dart';
import 'package:driver_share/app/data/remote_db/models/location_models/order_notification_resopnse.dart';
import 'package:driver_share/app/data/remote_db/request_body/order_notification_body.dart';
import 'package:get/get.dart';

import '../../../values/utilities/storage_pref.dart';

class OrderNotificationProvider extends GetConnect {
  static OrderNotificationProvider instance = OrderNotificationProvider();

  var duration = const Duration(seconds: 20);

  StoragePref storage = Get.find<StoragePref>();

  Future<OrderNotification> setOrder(providerUuid, providerName,
      customerName, phone, house, street, town, postcode, latitude,
      longitude) async =>
      await post(
          'http://192.168.0.108:4000/notification/send/order-notification',
          OrderNotificationBody.getOrder(
              providerUuid: providerUuid,
              providerName: providerName,
              customerName: customerName,
              phone: phone,
              house: house,
              street: street,
              town: town,
              postcode: postcode,
              latitude: latitude,
              longitude: longitude
          )
      ).then((response) {
        print("Send driver_notification response  =============> ${response.body}");
        if (response.statusCode == 200) {
          return OrderNotification.fromJson(response.body);
        } else if (response.statusCode == 404) {
          return OrderNotification.fromJson(response.body);
        } else {
          return OrderNotification.fromJson(response.body);
        }
      }).timeout(duration);
}