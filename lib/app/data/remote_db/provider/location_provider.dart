import 'package:driver_share/app/data/remote_db/models/auth_models/driver_login_response.dart';
import 'package:driver_share/app/data/remote_db/models/location_models/location_response.dart';
import 'package:get/get.dart';

import '../../../values/utilities/constants.dart';
import '../../../values/utilities/storage_pref.dart';
import '../models/location_models/get_location_response.dart';
import '../request_body/location_body.dart';

class LocationProvider extends GetConnect {
  static LocationProvider instance = LocationProvider();

  var duration = Duration(seconds: 20);

  StoragePref storage = Get.find<StoragePref>();

  Future<LocationResponse> sendLocation(
          driver_uuid, timestamp, latitude, longitude,heading,speed) async =>
      await post(
        '${Constants.BASE_URL}/location',
        LocationBody.getLocationBody(
          driver_uuid: driver_uuid,
          timestamp: timestamp,
          latitude: latitude,
          longitude: longitude,
          heading: heading,
          speed: speed,
        ),
        headers: {"DriverSession": storage.sessionId.val},
      )
          .then((response) {
            if (response.statusCode == 202) {
              return LocationResponse(true, "Success");
            } else if (response.statusCode == 403) {
              return LocationResponse(false, "Invalid DriverSession or UUID");
            } else if (response.statusCode == 500) {
              return LocationResponse(false, "Internal server error");
            } else {
              return LocationResponse(false, "Failed");
            }
          })
          .onError(
              (error, stackTrace) => LocationResponse(false, error.toString()))
          .catchError((error) => LocationResponse(false, error.toString()))
          .timeout(duration);



  Future<LocationRes> getLocationInfo() async => await get(
        '${Constants.BASE_URL}/location/query/driver/${storage.driverUuid.val}/last',
        headers: {"DriverSession": storage.sessionId.val},
      ).then((response) {
        return response.statusCode == 200
            ? LocationRes.formJson(response.body)
            : LocationRes.withError(response.body);
      }).onError((error, stackTrace) {
        return LocationRes.withError(
            {"error_code": "0", "error_description": error.toString()});
      }).timeout(duration);
}