import 'package:get/get.dart';
import '../../../values/utilities/constants.dart';
import '../../../values/utilities/storage_pref.dart';
import '../models/post_code_model/post_code_address_response.dart';
import '../models/post_code_model/post_code_response.dart';

class PostCodeProvider extends GetConnect {
  static PostCodeProvider instance = PostCodeProvider();

  var duration = const Duration(seconds: 20);

  StoragePref storage = Get.find<StoragePref>();



  Future<PostCodeResponse> getPostCode(postCode) async => await get(
    '${Constants.POSTCODE_ROOT_URL}/connector/postal_code/$postCode',
    headers: {"ProviderSession": storage.sessionId.val},
  ).then((response) {
    print("Post Code response==>${response.statusCode}");
    return response.statusCode == 200
    ? PostCodeResponse.fromJson(response.body):
    PostCodeResponse.withError({"message": "error"});
  }).timeout(duration);




  Future<PostCodeAddressResponse> getAddress(postCode) async => await get(
    '${Constants.POSTCODE_ROOT_URL}/connector/postal_data/$postCode',
  ).then((response) {
    print("Address response==>${response.statusCode}");
    return response.statusCode == 200
        ? PostCodeAddressResponse.fromJson(response.body)
        : PostCodeAddressResponse.withError(response.body);
  }).onError((error, stackTrace) {
    print("Error1==>$error");
    return PostCodeAddressResponse.withError(
        {"message": error.toString()});
  }).catchError((error) {
    print("Error2==>$error");
    return PostCodeAddressResponse.withError(
        {"message": error.toString()});
  }).timeout(duration);


}