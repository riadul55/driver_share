import 'dart:io';

import 'package:driver_share/app/data/remote_db/models/auth_models/driver_login_response.dart';
import 'package:driver_share/app/data/remote_db/models/auth_models/provider_login_response.dart';
import 'package:driver_share/app/data/remote_db/models/auth_models/driver_signup_response.dart';
import 'package:get/get.dart';

import '../../../values/utilities/constants.dart';
import '../models/auth_models/provider_signup_response.dart';
import '../request_body/auth_body.dart';

class AuthProvider extends GetConnect {
  static AuthProvider instance = AuthProvider();

  var duration = Duration(seconds: 20);

  Future<DriverLoginResponse> logIn(username, password) async => await post(
        '${Constants.BASE_URL}/driver/login',
        AuthBody.getLogInBody(
          username: username,
          password: password,
        ),
      ).then((response) {
        return response.statusCode == 200
            ? DriverLoginResponse.formJson(response.body)
            : DriverLoginResponse.withError(response.body);
      }).onError((error, stackTrace) {
        return DriverLoginResponse.withError(
            {"on error": "0", "error_description": error.toString()});
      }).catchError((error) {
        return DriverLoginResponse.withError(
            {"on error": "0", "error_description": error.toString()});
      }).timeout(duration);

  Future<DriverSignUpResponse> driverSignUp(email, password) async =>
      await post(
        '${Constants.BASE_URL}/driver/register',
        AuthBody.getSignUpBody(
          username: email,
          password: password,
          email: email,
          language: "en_GB",
        ),
      ).then((response) {
            if (response.statusCode == 201) {
              return DriverSignUpResponse(true, "Success");
            } else if (response.statusCode == 409) {
              return DriverSignUpResponse(false, "This username already exist!");
            } else {
              return DriverSignUpResponse(false, "Failed");
            }
          })
          .onError((error, stackTrace) =>
              DriverSignUpResponse(false, error.toString()))
          .catchError(
              (error) => DriverSignUpResponse(false, error.toString()))
          .timeout(duration);

  Future<ProviderLoginResponse> providerLogIn(username, password) async =>
      await post(
        '${Constants.BASE_URL}/provider/login',
        AuthBody.getLogInBody(
          username: username,
          password: password,
        ),
      ).then((response) {
            // if(response.statusCode==200){
            //   return ProviderLoginResponse.formJson(response.body);
            // }else if(response.statusCode==404){
            //   return ProviderLoginResponse.withError(response.body);
            // }
            return response.statusCode == 200
                ? ProviderLoginResponse.formJson(response.body)
                : ProviderLoginResponse.withError(response.body);
          })
          .onError((error, stackTrace) {
            return ProviderLoginResponse.withError(
                {"on error": "0", "error_description": error.toString()});
          })
          .catchError((error) => ProviderLoginResponse.withError({
                "catch error": "0",
                "message": error.toString(),
              }))
          .timeout(duration);

  Future<ProviderSignUpResponse> providerSignUp(email, password) async =>
      await post(
        '${Constants.BASE_URL}/provider/register',
        AuthBody.getSignUpBody(
          username: email,
          password: password,
          email: email,
          language: "en_GB",
        ),
      )
          .then((response) {
            print(response.statusCode);
            if (response.statusCode == 201) {

              return ProviderSignUpResponse(true, "Success");
            } else if (response.statusCode == 409) {
              return ProviderSignUpResponse(
                  false, "This username already exist!");
            } else {
              return ProviderSignUpResponse(false, "Failed");
            }
          })
          .onError((error, stackTrace) =>
              ProviderSignUpResponse(false, error.toString()))
          .catchError(
              (error) => ProviderSignUpResponse(false, error.toString()))
          .timeout(duration);

//
// Future<UserResponse> getUserInfo(token) async => await get(
//     '${Constants.BASE_URL}/customer-info',
//     headers: Constants.headers(token: token))
//     .then((response) => UserResponse.fromJson(response.body))
//     .onError((error, stackTrace) => UserResponse.withError(error.toString()))
//     .catchError((error) => UserResponse.withError(error.toString()))
//     .timeout(duration);
//
//
//
// Future<SendOtpResponse> sendSignUpOtp(phone) async => await post(
//     '${Constants.BASE_URL}/sign-up-otp',
//     AuthBody.getSignUpOtpBody(phone: phone),
//     headers: Constants.headers())
//     .then((response) => SendOtpResponse.fromJson(response.body))
//     .onError((error, stackTrace) => SendOtpResponse.fromJson({
//   "status": "0",
//   "message": error.toString(),
// }))
//     .catchError((error) => SendOtpResponse.fromJson({
//   "status": "0",
//   "message": error.toString(),
// }))
//     .timeout(duration);
//
// Future<SendOtpResponse> sendForgetPassOtp(phone) async => await post(
//     '${Constants.BASE_URL}/forgot-password-otp',
//     AuthBody.getSignUpOtpBody(phone: phone),
//     headers: Constants.headers())
//     .then((response) => SendOtpResponse.fromJson(response.body))
//     .onError((error, stackTrace) => SendOtpResponse.fromJson({
//   "status": "0",
//   "message": error.toString(),
// }))
//     .catchError((error) => SendOtpResponse.fromJson({
//   "status": "0",
//   "message": error.toString(),
// }))
//     .timeout(duration);
//
//
// Future<SendOtpResponse> updatePassword(password) async => await post(
//     '${Constants.BASE_URL}/forgot-password-update',
//     AuthBody.getChangePassBody(password: password),
//     headers: Constants.headers())
//     .then((response) => SendOtpResponse.fromJson(response.body))
//     .onError((error, stackTrace) => SendOtpResponse.fromJson({
//   "status": "0",
//   "message": error.toString(),
// }))
//     .catchError((error) => SendOtpResponse.fromJson({
//   "status": "0",
//   "message": error.toString(),
// }))
//     .timeout(duration);
//
//
//
// Future<AuthResponse> updateProfile(
//     fullName, email, gender, birthday, token) async =>
//     await post(
//         '${Constants.BASE_URL}/profile-update',
//         AuthBody.getUpdateProfileBody(
//           fullName: fullName,
//           email: email,
//           gender: gender,
//           birthday: birthday,
//         ),
//         headers: Constants.headers(token: token))
//         .then((response) => AuthResponse.fromJson(response.body))
//         .onError((error, stackTrace) => AuthResponse.fromJson({
//       "status": "0",
//       "message": error.toString(),
//     }))
//         .catchError((error) => AuthResponse.fromJson({
//       "status": "0",
//       "message": error.toString(),
//     }))
//         .timeout(duration);
//
// Future<AuthResponse> updateProfilePic(File photo, token) async {
//   final form =
//   FormData({'user_photo': MultipartFile(photo, filename: 'profile.jpg')});
//   return await post('${Constants.BASE_URL}/profile-update', form,
//       headers: Constants.headers(token: token))
//       .then((response) => AuthResponse.fromJson(response.body))
//       .onError((error, stackTrace) => AuthResponse.fromJson({
//     "status": "0",
//     "message": error.toString(),
//   }))
//       .catchError((error) => AuthResponse.fromJson({
//     "status": "0",
//     "message": error.toString(),
//   }))
//       .timeout(duration);
// }
//
//
// Future<AuthResponse> changeNewPassword(token, oldPass, newPass) async {
//   return await post('${Constants.BASE_URL}/change-password', {
//     "old_pass" : "$oldPass",
//     "new_psss" : "$newPass",
//     "con_pass" : "$newPass",
//   },
//       headers: Constants.headers(token: token))
//       .then((response) => AuthResponse.fromJson(response.body))
//       .onError((error, stackTrace) => AuthResponse.fromJson({
//     "status": "0",
//     "message": error.toString(),
//   }))
//       .catchError((error) => AuthResponse.fromJson({
//     "status": "0",
//     "message": error.toString(),
//   }))
//       .timeout(duration);
// }

// Future<void> signOut() async {}
}
