import 'package:get/get.dart';

import '../../../values/utilities/constants.dart';
import '../../../values/utilities/storage_pref.dart';
import '../models/find_driver_model/find_driver_response.dart';

class FindDriverProvider extends GetConnect {
  static FindDriverProvider instance = FindDriverProvider();

  var duration = Duration(seconds: 20);

  StoragePref storage = Get.find<StoragePref>();



  Future<FindDriverResponse> getDriverLocation(latitude,longitude, radix) async => await get(
    '${Constants.BASE_URL}/location/find/driver',
    query: {
      "lon": "$longitude",
      "lat": "$latitude",
      "radix": "$radix",
    },headers: {"ProviderSession": storage.sessionId.val},
  ).then((response) {
    print("response find driver url==>${response.request!.url.toString()}");
    print("response find driver==>${response.body}");
    return response.statusCode == 200
        ? FindDriverResponse.fromJson(response.body)
        : FindDriverResponse.withError(response.body);
  }).onError((error, stackTrace) {
    print("Error1==>$error");
    return FindDriverResponse.withError(
        {"on error": "0", "error_description": error.toString()});
  }).catchError((error) {
    print("Error2==>$error");
    return FindDriverResponse.withError(
        {"on error": "0", "error_description": error.toString()});
  }).timeout(duration);

}