import 'package:driver_share/app/values/utilities/constants.dart';

class OrderNotificationBody {
  static Map<String, dynamic> getOrder(
          {providerUuid, providerName,customerName,phone,house,street,town,postcode, latitude, longitude,}) =>
      {
        "provider": {
          "uuid": "$providerUuid",
          "name": "$providerName"
        },
        "customer": {
          "name": "$customerName",
          "phone": "$phone",
          "house": "$house",
          "street": "$street",
          "town": "$town",
          "postcode": "$postcode",
          "latitude": "$latitude",
          "longitude": "$longitude"
        },
      };
}
