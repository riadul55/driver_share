import 'package:driver_share/app/values/utilities/constants.dart';

class AuthBody {
  static Map<String, String> getLogInBody({username, password}) => {
    "username": "$username",
    "password": "$password",
  };

  static Map<String, String> getSignUpBody({username, password,email,language}) => {
    "username": "$username",
    "password": "$password",
    "email": "$email",
    "language": "$language",
  };



  static Map<String, String> getChangePassBody({password}) => {
    "password": "$password",
  };

}
