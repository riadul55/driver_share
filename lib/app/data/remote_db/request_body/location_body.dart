import 'package:driver_share/app/values/utilities/constants.dart';

class LocationBody {
  static Map<String, dynamic> getLocationBody({driver_uuid, timestamp, latitude, longitude,heading,speed}) => {
    "driver_uuid": "$driver_uuid",
    "timestamp": "$timestamp",
    "location" : {
      "latitude": "$latitude",
      "longitude": "$longitude"
    },
    "heading" : "$heading",
    "speed" :"$speed"
  };

}