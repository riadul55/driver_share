import 'package:awesome_notifications/awesome_notifications.dart';

Future<void> createNotificationWithImage() async{
  await AwesomeNotifications().createNotification(
      content: NotificationContent(
        id: DateTime.now().millisecondsSinceEpoch.remainder(100000),
        channelKey: "basic_channel",
        title: "Awesome Notification",
        body: "This is basic driver_notification body.",
        bigPicture: 'asset://assets/icons/awesome.png',
        notificationLayout: NotificationLayout.BigPicture,
        ));
}
Future<void> createNotificationWithActionButton({Map<String, String>? data, required int uId}) async{
  data!["uuid"] = "$uId";
  await AwesomeNotifications().createNotification(
      content: NotificationContent(
        autoDismissible: false,
        id: uId,
        // id: DateTime.now().millisecondsSinceEpoch.remainder(100000),
        channelKey: "call_channel",
        title: "Awesome Notification",
        body: "This is basic driver_notification body.",
        payload: data,
        ),
    actionButtons: [
      NotificationActionButton(key: "Accept", label: "accept"),
      NotificationActionButton(key: "Decline", label: "decline"),
    ]

  );
}




int createUniqueId(){
  return DateTime.now().millisecondsSinceEpoch.remainder(100000);
}

  void createNotification(String title, String body, String channelKey){
    AwesomeNotifications().createNotification(
        content: NotificationContent(
          id: 1,
          channelKey: channelKey,
          title: title,
          body: body,
          // bigPicture: 'assets/icons/car1.png',
          // notificationLayout: NotificationLayout.Default,
        ));
  }

