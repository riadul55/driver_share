import 'package:flutter/cupertino.dart';


import 'package:get/get.dart';

class ForgotPasswordController extends GetxController {
  // final AuthRepository _authRepository;
  // final _loadingController = Get.find<LoadingController>();
  //
  // ForgotPasswordController({required AuthRepository authRepository})
  //     : _authRepository = authRepository;

  @override
  void onInit() {
    super.onInit();

    ever<String>(new_pass, validatePassword);
    ever<String>(re_pass, validateRePassword);
  }

  Future<void> forgetPass() async {}

  bool validateFields() {
    validatePassword(new_pass.value);
    validateRePassword(re_pass.value);

    return new_pass.isNotEmpty &&
        re_pass.isNotEmpty &&
        new_pass.value == null &&
        re_pass.value == null;
  }

  final new_pass = ''.obs;
  final new_passError = RxnString();
  final new_passFocus = FocusNode();

  void validatePassword(String val) {
    if (val.isEmpty) {
      new_passError.value = 'Enter user name';
    } else if (val.length < 3) {
      new_passError.value = 'invalid user name';
    } else {
      new_passError.value = null;
    }
  }

  final re_pass = ''.obs;
  final re_passError = RxnString();
  final re_passFocus = FocusNode();

  void validateRePassword(String val) {
    if (val.isEmpty) {
      re_passError.value = 'Enter user name';
    } else if (val.length < 3) {
      re_passError.value = 'invalid user name';
    } else {
      re_passError.value = null;
    }
  }

  bool get enableButton =>
      new_pass.isNotEmpty &&
      re_pass.isNotEmpty &&
      new_passError.value != null &&
      re_passError.value != null;
}
