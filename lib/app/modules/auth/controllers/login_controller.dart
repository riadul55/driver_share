
import 'package:driver_share/app/modules/global_components/global_locaton_alert.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../data/remote_db/models/auth_models/driver_login_response.dart';
import '../../../data/remote_db/models/auth_models/provider_login_response.dart';
import '../../../data/remote_db/models/location_models/position_model.dart';
import '../../../data/remote_db/provider/auth_provider.dart';
import '../../../routes/app_pages.dart';
import '../../../values/utilities/storage_pref.dart';
import '../../global_components/controllers/loading_controller.dart';
import '../../global_components/controllers/location_controller.dart';
import '../../global_components/snack_bar.util.dart';

class LoginController extends GetxController {
  final _loadingController = Get.find<LoadingController>();
  final LocationController locationController = Get.find<LocationController>();
  late final AuthProvider _authenticationProvider;
  StoragePref storage = Get.find<StoragePref>();
  var radioButtonValue = 1.obs;

  @override
  void onInit() {
    super.onInit();
    ever<String>(login, validateLogin);
    ever<String>(password, validatePassword);
    _authenticationProvider = AuthProvider();
    print(storage.isLoggedIn.val);
  }

  Future<void> doDriverLogin(String login, String password) async {
    _loadingController.isLoading = true;
    await _authenticationProvider
        .logIn(login, password)
        .then((DriverLoginResponse response) {
      if (response != null && response.loginSuccessResponse != null) {
        storage.isLoggedIn.val = true;
        storage.isDriverLoggedIn.val = true;
        storage.sessionId.val =
            response.loginSuccessResponse!.session_id.toString();
        storage.driverUuid.val =
            response.loginSuccessResponse!.driver_uuid.toString();
        storage.loginID.val=login.toString();
        _loadingController.isLoading = false;
        print('sessionId====> ${response.loginSuccessResponse!.session_id}');
        print('DriverUuid====> ${response.loginSuccessResponse!.driver_uuid}');
        Get.offAllNamed(Routes.DRIVER_HOME);
      } else if (response != null && response.driverErrorResponse != null) {
        _loadingController.isLoading = false;
        SnackbarUtil.showError(
            message: "${response.driverErrorResponse!.error_description}");
      } else {
        _loadingController.isLoading = false;
        SnackbarUtil.showError(message: "Something went wrong!");
      }
    });
  }

  Future<void> doProviderLogin(String login, String password) async {
    _loadingController.isLoading = true;
    await _authenticationProvider
        .providerLogIn(login, password)
        .then((ProviderLoginResponse response) {
      if (response != null && response.providerSuccessResponse != null) {
        storage.isLoggedIn.val = true;
        storage.isDriverLoggedIn.val = false;
        storage.sessionId.val =
            response.providerSuccessResponse!.session_id.toString();
        storage.providerUuid.val =
            response.providerSuccessResponse!.provider_uuid.toString();
        storage.loginID.val=login.toString();
        _loadingController.isLoading = false;
        print('sessionId====> ${response.providerSuccessResponse!.session_id}');
        print('ProviderUuid====> ${response.providerSuccessResponse!
            .provider_uuid}');
        Get.offAllNamed(Routes.PROVIDER_HOME);
        // ever(locationController.currentPosition, locationProcess);
      } else if (response != null && response.providerErrorResponse != null) {
        _loadingController.isLoading = false;
        SnackbarUtil.showError(
            message: "${response.providerErrorResponse!.error_description}");
      } else {
        _loadingController.isLoading = false;
        SnackbarUtil.showError(message: "Something went wrong!");
      }
    });
  }

  // locationProcess(Position position) {
  //   if(position.longitude!=null && position.longitude!=null){
  //     Get.offAllNamed(Routes.PROVIDER_HOME);
  //   }
  // }

  bool validateFields() {
    validateLogin(login.value);
    validatePassword(password.value);

    return login.isNotEmpty &&
        password.isNotEmpty &&
        loginError.value == null &&
        passwordError.value == null;
  }

  final login = ''.obs;
  final loginError = RxnString();
  final loginFocus = FocusNode();

  void validateLogin(String val) {
    if (val.isEmpty) {
      loginError.value = 'Enter user name';
    } else if (val.length < 10) {
      loginError.value = 'invalid user name';
    } else {
      loginError.value = null;
    }
  }

  final password = ''.obs;
  final passwordError = RxnString();
  final passwordFocus = FocusNode();

  void validatePassword(String val) {
    if (val.isEmpty) {
      passwordError.value = 'Enter password';
    } else if (val.length < 8) {
      passwordError.value = 'Password must be at least 8 characters';
    } else {
      passwordError.value = null;
    }
  }

  void textCheck() {
    if (login.value.isEmpty || password.value.isEmpty) {
      SnackbarUtil.showWarning(message: "Please fill all information!");
      storage.isLoggedIn.val = true;
      print(storage.isLoggedIn.val);
    } else if (login.value.isNotEmpty &&
        password.value.isNotEmpty &&
        loginError.value == null &&
        passwordError.value == null) {
      if (radioButtonValue.value == 1) {
        checkPermission(1);
      } else if (radioButtonValue.value == 2) {
        checkPermission(2);
      }
    }
  }

  void checkPermission(int value) async {
    bool checkPermission = await locationController.checkPermission();
    if (checkPermission) {
      if(value==1){
        doDriverLogin(login.value, password.value);
      }else if(value==2){
        doProviderLogin(login.value, password.value);
      }
    } else {
      GlobalLocationAlert();
    }
  }


  showAlertDialog() {

    // set up the button
    Widget okButton = TextButton(
      child: Text("OK"),
      onPressed: () { Get.back(); },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: const Text("Location Permission"),
      content: const Text("For better experience turn on the location permission."),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    Get.dialog(alert, barrierDismissible: true);
  }

}


