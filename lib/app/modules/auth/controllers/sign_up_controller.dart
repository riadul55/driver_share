import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import '../../../data/remote_db/models/auth_models/driver_signup_response.dart';
import '../../../data/remote_db/models/auth_models/provider_signup_response.dart';
import '../../../data/remote_db/provider/auth_provider.dart';
import '../../../routes/app_pages.dart';
import '../../global_components/controllers/loading_controller.dart';
import '../../global_components/snack_bar.util.dart';

class SignUpController extends GetxController {
  final _loadingController = Get.find<LoadingController>();

  late final AuthProvider _authenticationProvider;
  var storage = GetStorage();
  var radioButtonValue = 1.obs;

  @override
  void onInit() {
    super.onInit();
    ever<String>(email, validateEmail);
    ever<String>(password, validatePassword);
    ever<String>(con_pass, validateCon_Pass);
    _authenticationProvider = AuthProvider();
  }

  Future<void> doDriverSignUp(String email, String password) async {
    _loadingController.isLoading = true;
    await _authenticationProvider
        .driverSignUp(email, password)
        .then((DriverSignUpResponse response) {
      if (response.success) {
        SnackbarUtil.showSuccess(message: "Successfully Sign Up");
        _loadingController.isLoading = false;
        Get.offAllNamed(Routes.LOGIN);
      } else {
        _loadingController.isLoading = false;
        SnackbarUtil.showSuccess(message: response.message);
      }
    });
  }

  Future<void> doProviderSignUp(String email, String password) async {
    _loadingController.isLoading = true;
    await _authenticationProvider
        .providerSignUp(email, password)
        .then((ProviderSignUpResponse response) {
      if (response.success) {
        SnackbarUtil.showSuccess(message: "Successfully Sign Up");
        _loadingController.isLoading = false;
        Get.offAllNamed(Routes.LOGIN);
      } else {
        print("false");
        _loadingController.isLoading = false;
        SnackbarUtil.showSuccess(message: response.message);
      }
    });
  }

  bool validateFields() {
    validateEmail(email.value);
    validatePassword(password.value);
    validateCon_Pass(con_pass.value);

    return email.isNotEmpty &&
        password.isNotEmpty &&
        con_pass.isNotEmpty &&
        emailError.value == null &&
        passwordError.value == null &&
        con_pass.value == null;
  }

  final email = ''.obs;
  final emailError = RxnString();
  final emailFocus = FocusNode();

  void validateEmail(String val) {
    if (val.isEmpty) {
      emailError.value = 'Enter e-mail';
    } else if (val.length < 10) {
      emailError.value = 'invalid e-mail';
    } else {
      emailError.value = null;
    }
  }

  final password = ''.obs;
  final passwordError = RxnString();
  final passwordFocus = FocusNode();

  void validatePassword(String val) {
    if (val.isEmpty) {
      passwordError.value = 'Enter password';
    } else if (val.length < 8) {
      passwordError.value = 'Password must be at least 8 characters';
    } else {
      passwordError.value = null;
    }
  }

  final con_pass = ''.obs;
  final con_passError = RxnString();
  final con_passFocus = FocusNode();

  void validateCon_Pass(String val) {
    if (val.isEmpty) {
      con_passError.value = 'Enter confirm password';
    } else if (val != password.value) {
      con_passError.value = 'Password does not match';
    } else {
      con_passError.value = null;
    }
  }

  void textCheck() {
    if (email.value.isEmpty ||
        password.value.isEmpty ||
        con_pass.value.isEmpty) {
      SnackbarUtil.showWarning(message: "Please fill all information!");
    } else if (email.value.isNotEmpty &&
        password.value.isNotEmpty &&
        con_pass.value.isNotEmpty &&
        emailError.value == null &&
        passwordError.value == null &&
        con_passError.value == null) {
      if (radioButtonValue.value == 1) {
        doDriverSignUp(email.value, password.value);
      } else if (radioButtonValue.value == 2) {
        doProviderSignUp(email.value, password.value);
      }
    }
  }
}
