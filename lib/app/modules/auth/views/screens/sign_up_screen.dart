import '../../../../routes/app_pages.dart';
import '../../../global_components/base_widget.dart';
import '../../../global_components/views/global_button_widget.dart';
import '../../../global_components/views/global_password_text_field_widget.dart';
import '../../../global_components/views/global_text_field_widget.dart';
import '../../../global_components/views/or_devider.dart';
import '../../controllers/sign_up_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SignUpScreen extends GetView<SignUpController> {
  @override
  Widget build(BuildContext context) {
    Size size=MediaQuery.of(context).size;
    if(size.width>480 && size.width<=1280){
      //For Tab
    return BaseWidget(
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.all(30),
          child: Center(
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.only(top: 25, bottom: 5),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Sign Up',
                      style: TextStyle(
                          fontSize: 20, color: Theme.of(context).colorScheme.onPrimary,),
                    ),
                    const SizedBox(height: 50),
                    Obx(
                          () => SizedBox(
                            width: size.width*.4,
                            child: GlobalTextFieldWidget(
                        hintText: "E-mail",
                        onChanged: controller.email,
                        errorText: controller.emailError.value,
                        focusNode: controller.emailFocus,
                        onFieldSubmit: (_) {
                            controller.passwordFocus.requestFocus();
                        },
                      ),
                          ),
                    ),
                    const SizedBox(height: 10),
                    Obx(() => SizedBox(
                      width: size.width*.4,
                      child: GlobalPasswordFieldWidget(
                        hintText: "Password",
                        onChange: controller.password,
                        errorText: controller.passwordError.value,
                        focusNode: controller.passwordFocus,
                        onFieldSubmit: (_) {
                          controller.con_passFocus.requestFocus();
                        } ,
                      ),
                    )),
                    const SizedBox(height: 10),
                    Obx(() => SizedBox(
                      width: size.width*.4,
                      child: GlobalPasswordFieldWidget(
                        hintText: "Confirm Password",
                        onChange: controller.con_pass,
                        errorText: controller.con_passError.value,
                        focusNode: controller.con_passFocus,
                        onFieldSubmit: (_) => controller.textCheck(),
                      ),
                    )),
                    const SizedBox(height: 15),
                    SignUpRadioButton(
                      widthSize: size.width,
                      containerWidth: 5,
                      controller: controller,),
                    const SizedBox(height: 15,),
                    SizedBox(
                      width: size.width*.4,
                      child: GlobalButtonWidget(
                        text: "Sign Up",
                        pres:() {
                          controller.textCheck();
                          FocusScope.of(context).unfocus();
                        }

                      ),
                    ),
                    const SizedBox(height: 30),
                    OrDevider(lineWidth:size.width *.4),
                    const SizedBox(height: 10),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: TextButton(
                        // onPressed: () {Navigator.pop(context);},
                        onPressed: () {Navigator.pop(context);},
                        child: Text(
                          "Log In",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Theme.of(context).colorScheme.onPrimary,
                              fontSize: 16),
                        ),
                      ),
                    ),
                    const SizedBox(height: 10),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
    }

    else {
      //For Mobile
      return BaseWidget(
        child: Scaffold(
          body: Padding(
            padding: const EdgeInsets.all(30),
            child: Center(
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.only(top: 30, bottom: 30),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Sign Up',
                        style: TextStyle(
                          fontSize: 20, color: Theme.of(context).colorScheme.onPrimary,),
                      ),
                      const SizedBox(height: 50),
                      Obx(
                            () => GlobalTextFieldWidget(
                          hintText: "E-mail",
                          onChanged: controller.email,
                          errorText: controller.emailError.value,
                          focusNode: controller.emailFocus,
                          onFieldSubmit: (_) {
                            controller.passwordFocus.requestFocus();
                          },
                        ),
                      ),
                      const SizedBox(height: 10),
                      Obx(() => GlobalPasswordFieldWidget(
                        hintText: "Password",
                        onChange: controller.password,
                        errorText: controller.passwordError.value,
                        focusNode: controller.passwordFocus,
                        onFieldSubmit: (_) {
                          controller.con_passFocus.requestFocus();
                        } ,
                      )),
                      const SizedBox(height: 10),
                      Obx(() => GlobalPasswordFieldWidget(
                        hintText: "Confirm Password",
                        onChange: controller.con_pass,
                        errorText: controller.con_passError.value,
                        focusNode: controller.con_passFocus,
                        onFieldSubmit: (_) => controller.textCheck(),
                      )),
                      const SizedBox(height: 15),
                      SignUpRadioButton(
                        widthSize: size.width,
                        containerWidth: 2.4,
                        controller: controller,),
                      const SizedBox(height: 15,),
                      GlobalButtonWidget(
                          text: "Sign Up",
                          pres:() {
                            controller.textCheck();
                            FocusScope.of(context).unfocus();
                          }

                      ),
                      const SizedBox(height: 30),
                      OrDevider(lineWidth: size.width*.8,),
                      const SizedBox(height: 10),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: TextButton(
                          // onPressed: () {Navigator.pop(context);},
                          onPressed: () {Navigator.pop(context);},
                          child: Text(
                            "Log In",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Theme.of(context).colorScheme.onPrimary,
                                fontSize: 16),
                          ),
                        ),
                      ),
                      const SizedBox(height: 10),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      );
    }
  }
}



class SignUpRadioButton extends StatelessWidget {
  final SignUpController controller;
  final double widthSize;
  final double containerWidth;

  const SignUpRadioButton({Key? key, required this.controller, required this.widthSize, required this.containerWidth})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SizedBox(width: widthSize,
      child: Row(
        mainAxisAlignment:MainAxisAlignment.center,
        children: [
          SizedBox(
            width: widthSize / containerWidth,
            height: 50,
            child: ListTile(
              title:  Text("Driver",style: TextStyle(color: Theme.of(context).colorScheme.onPrimary),),
              leading: Obx(
                    () => Radio(
                  value: 1,
                  groupValue: controller.radioButtonValue.value,
                  onChanged: (int? value) {
                    controller.radioButtonValue.value = value!;
                  }, activeColor: Theme.of(context).colorScheme.primaryContainer,
                ),
              ),
            ),
          ),
          SizedBox(
            width: widthSize / containerWidth,
            height: 50,
            child: ListTile(
              title:  Text("Business",style: TextStyle(color: Theme.of(context).colorScheme.onPrimary),),
              leading: Obx(
                    () => Radio(
                  value: 2,
                  groupValue: controller.radioButtonValue.value,
                  onChanged: (int? value) {
                    controller.radioButtonValue.value = value!;
                  }, activeColor: Theme.of(context).colorScheme.primaryContainer,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}