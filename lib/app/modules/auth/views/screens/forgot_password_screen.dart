import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../global_components/base_widget.dart';
import '../../../global_components/views/global_button_widget.dart';
import '../../../global_components/views/global_password_text_field_widget.dart';
import '../../controllers/forgot_password_controller.dart';

class ForgotPasswordScreen extends GetView<ForgotPasswordController> {
  @override
  Widget build(BuildContext context) {
    Size size=MediaQuery.of(context).size;
  if(size.width>480 && size.width<=1280){
    //For Tab
    return BaseWidget(
        child: Scaffold(
          body: Padding(
            padding: const EdgeInsets.all(30),
            child: Center(
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.only(top: 25, bottom: 35),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Find your account',
                        style: TextStyle(
                            fontSize: 20, color: Theme.of(context).colorScheme.onPrimary,),
                      ),
                      const SizedBox(height: 50),
                      Obx(
                            () => SizedBox(
                              width: size.width*.4,
                              child: GlobalPasswordFieldWidget(
                          hintText: "New Password",
                          onChange: controller.new_pass,
                          errorText: controller.new_passError.value,
                          focusNode: controller.re_passFocus,
                          onFieldSubmit: (_) {
                              controller.new_passFocus.requestFocus();
                          },
                        ),
                            ),
                      ),
                      const SizedBox(height: 10),
                      Obx(() => SizedBox(
                        width: size.width*.4,
                        child: GlobalPasswordFieldWidget(
                          hintText: "Re-Password",
                          onChange: controller.re_pass,
                          errorText: controller.re_passError.value,
                          onFieldSubmit: (_) => controller.forgetPass(),
                        ),
                      )),
                      const SizedBox(height: 20),
                      SizedBox(
                        width: size.width*.4,
                        child: GlobalButtonWidget(
                          text: "CONFIRM",
                          pres: (){
                            FocusScope.of(context).unfocus();
                          }
                        ),
                      ),

                      const SizedBox(height: 10),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
    );
  }

  else{
    //For Mobile
    return BaseWidget(
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.all(30),
          child: Center(
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.only(top: 30, bottom: 30),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Find your account',
                      style: TextStyle(
                        fontSize: 20, color: Theme.of(context).colorScheme.onPrimary,),
                    ),
                    const SizedBox(height: 50),
                    Obx(
                          () => GlobalPasswordFieldWidget(
                        hintText: "New Password",
                        onChange: controller.new_pass,
                        errorText: controller.new_passError.value,
                        focusNode: controller.re_passFocus,
                        onFieldSubmit: (_) {
                          controller.new_passFocus.requestFocus();
                        },
                      ),
                    ),
                    const SizedBox(height: 10),
                    Obx(() => GlobalPasswordFieldWidget(
                      hintText: "Re-Password",
                      onChange: controller.re_pass,
                      errorText: controller.re_passError.value,
                      onFieldSubmit: (_) => controller.forgetPass(),
                    )),
                    const SizedBox(height: 20),
                    GlobalButtonWidget(
                        text: "CONFIRM",
                        pres: (){
                          FocusScope.of(context).unfocus();
                        }
                    ),

                    const SizedBox(height: 10),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
  }
}
