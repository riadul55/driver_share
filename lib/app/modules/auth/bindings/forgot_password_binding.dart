import 'package:driver_share/app/modules/auth/controllers/forgot_password_controller.dart';
import 'package:get/get.dart';

import '../controllers/auth_controller.dart';

class ForgotPasswordBinding
    extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ForgotPasswordController>(
          () => ForgotPasswordController(),
    );
  }
}