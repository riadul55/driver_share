import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/provider_setting_controller.dart';

class ProviderSettingView extends GetView<ProviderSettingController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('ProviderSettingView'),
        centerTitle: true,
      ),
      body: const Center(
        child: Text(
          'ProviderSettingView is working',
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}
