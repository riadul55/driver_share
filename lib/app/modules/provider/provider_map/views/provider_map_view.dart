import 'package:driver_share/app/modules/provider/provider_map/views/screen/provider_map_body.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../global_components/base_widget.dart';
import '../controllers/provider_map_controller.dart';

class ProviderMapView extends GetView<ProviderMapController> {
  const ProviderMapView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseWidget(
      child: Scaffold(
        appBar: AppBar(
            title: Text('${controller.radius.value}m'),
            centerTitle: false,
            ),
        body: ProviderMapBody(
          controller: controller,
        ),
        floatingActionButtonLocation:
            FloatingActionButtonLocation.miniStartFloat,
        floatingActionButton: FloatingActionButton(
          // isExtended: true,
          child: const Icon(
            Icons.location_searching,
            color: Colors.white,
          ),
          backgroundColor: Theme.of(context).colorScheme.primary,
          onPressed: () {
            controller.updatePosition();
          },
        ),
      ),
    );
  }
}







