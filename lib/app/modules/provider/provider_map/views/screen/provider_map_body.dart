import 'package:driver_share/app/modules/provider/provider_map/controllers/provider_map_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class ProviderMapBody extends StatelessWidget {
  final ProviderMapController controller;
  ProviderMapBody({Key? key, required this.controller}) : super(key: key);


  late GoogleMapController googleMapController;
  CameraPosition initialPosition = const CameraPosition(
    zoom: 10,
    tilt: 0,
    bearing: 0,
    target: LatLng(23.3269078, 90.1711455),
  );

  @override
  Widget build(BuildContext context) {
    return Obx(() => Container(
      child: GoogleMap(
        // polylines: controller.polylines,
        mapType: MapType.normal,
        markers: Set<Marker>.of(controller.marker),
        onMapCreated: controller.onMapCreated,
        // onMapCreated: (GoogleMapController ctr){
        //googleMapController=ctr;
        // },
        initialCameraPosition: initialPosition,
        circles: controller.circles,
        // circles: Set<Circle>.of([controller.c]),
      ),
    ));
  }
}
