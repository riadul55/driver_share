import 'dart:async';
import 'package:driver_share/app/modules/provider/provider_home/controllers/provider_home_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'dart:math';
import 'package:location/location.dart';

import '../../../../data/remote_db/models/find_driver_model/find_driver_response.dart';
import '../../../../data/remote_db/models/location_models/position_model.dart';
import '../../../../data/remote_db/provider/find_driver_provider.dart';
import '../../../../data/remote_db/provider/location_provider.dart';
import '../../../../values/utilities/storage_pref.dart';
import '../../../global_components/controllers/loading_controller.dart';
import '../../../global_components/controllers/location_controller.dart';
import '../../../global_components/snack_bar.util.dart';

class ProviderMapController extends GetxController {
  StoragePref storage = Get.find<StoragePref>();
  final _loadingController = Get.find<LoadingController>();
  late GoogleMapController googleMapController;
  final LocationController locationController = Get.find<LocationController>();
  final ProviderHomeController providerHomeController =
      Get.find<ProviderHomeController>();
  Completer<GoogleMapController> _completerontroller = Completer();
  late LocationProvider locationProvider;
  late FindDriverProvider findDriverProvider;

  var currentPosition = Position().obs;

  late StreamSubscription<LocationData> subscription;
  late LocationData currentLocation;

  List<Placemark> place = [];

  // var markerValue = <Marker>[].obs;
  List<Marker> marker = <Marker>[].obs;
  late BitmapDescriptor mapMarker;
  List<FindSuccessResponse> drivers = <FindSuccessResponse>[].obs;
  var currentLat = 0.0.obs;
  var currentLng = 0.0.obs;
  var radius = "500".obs;
  Timer? timer;
  late Set<Circle> circles = <Circle>{}.obs;
  @override
  void onInit() {
    locationProvider = LocationProvider();
    findDriverProvider = FindDriverProvider();
    currentLat.value = Get.arguments["lat"];
    currentLng.value = Get.arguments["lng"];
    radius.value = Get.arguments['radius'];
    drivers = Get.arguments['drivers'];
    getPlaceMark();

    timer = Timer.periodic(
        const Duration(seconds: 10), (Timer t) => updatePosition());

    circles = { Circle(
      circleId: CircleId(storage.providerUuid.val),
      center: LatLng(currentLat.value, currentLng.value),
      strokeColor: const Color(0xFF655DB0),
      strokeWidth: 1,
      fillColor: const Color(0x1A9A96FF),
      radius: double.parse(radius.value.toString()),
    )};
    super.onInit();
  }

  double getZoomLevel(double radius) {
    double zoomLevel = 11;
    if (radius > 0) {
      double radiusElevated = radius + radius / 2;
      double scale = radiusElevated / 500;
      zoomLevel = 16.35 - log(scale) / log(2);
    }
    // zoomLevel = num.parse(zoomLevel.toStringAsFixed(2));
    zoomLevel = double.parse(zoomLevel.toStringAsFixed(2));
    return zoomLevel;
  }

  /*void onMapCreated(GoogleMapController controller) {
    _completerontroller.complete(controller);
    print("map create");

      BitmapDescriptor.fromAssetImage(
              ImageConfiguration(), 'assets/icons/car2.png')
          .then((value) {
        marker.add(Marker(
            markerId: MarkerId(storage.providerUuid.val),
            position: LatLng(currentLat.value, currentLng.value),
            infoWindow: InfoWindow(
                title: storage.providerUuid.val, snippet:"${place[0].street}, "
                "${place[0].subLocality}, ""${place[0].locality}"),
        ),);
        drivers.forEach((FindSuccessResponse driver) {
          marker.add(Marker(
              markerId: MarkerId('${driver.driverUuid}'),
              icon: value,
              rotation: driver.heading!,
              position: LatLng(driver.latitude ?? 0.0, driver.longitude ?? 0.0),
              infoWindow: InfoWindow(
                  title: '${driver.driverUuid}',
                  snippet: 'Dhaka, Bangladesh'),
          ),);
        });
        CameraPosition cameraPosition = CameraPosition(
            zoom: 16,
            tilt: 0,
            bearing: 0,
            target: LatLng(currentLat.value, currentLng.value));
        controller
            .animateCamera(CameraUpdate.newCameraPosition(cameraPosition));
        // updatePosition();
      });


  }*/

  void onMapCreated(GoogleMapController controller) {
    _completerontroller.complete(controller);
    print("map create");
    // updatePosition();
  }

  void updatePosition() async {
    print("printed update position method");
    // currentLat.value = providerHomeController.currentLat.value;
    // currentLng.value = providerHomeController.currentLng.value;
    var sourcePosition = LatLng(currentLat.value, currentLng.value);
    final GoogleMapController ctr = await _completerontroller.future;

    CameraPosition cameraPosition =
        CameraPosition(zoom: getZoomLevel(double.parse(radius.value)), tilt: 0, bearing: 0, target: sourcePosition);
    ctr.animateCamera(CameraUpdate.newCameraPosition(cameraPosition));

    // marker.removeWhere((item) => item.markerId.value == storage.providerUuid.val);

    circles = { Circle(
      circleId: CircleId(storage.providerUuid.val),
      center: LatLng(currentLat.value, currentLng.value),
      strokeColor: const Color(0xFF655DB0),
      strokeWidth: 1,
      fillColor: const Color(0x1A9A96FF),
      radius: double.parse(radius.value.toString()),
    )};

    await findDriverProvider
        .getDriverLocation(currentLng.value, currentLat.value, double.parse(radius.value))
        .then((FindDriverResponse response) {
      if (response != null &&
          response.findSuccessResponse != null &&
          response.findSuccessResponse!.isNotEmpty) {
        marker.clear();
        drivers = response.findSuccessResponse!.toList();
        BitmapDescriptor.fromAssetImage(
                ImageConfiguration(), 'assets/icons/car2.png')
            .then((value) {
          drivers.forEach((FindSuccessResponse driver) {
            marker.add(
              Marker(
                markerId: MarkerId('${driver.driverUuid}'),
                icon: value,
                rotation: driver.heading!,
                position:
                    LatLng(driver.latitude ?? 0.0, driver.longitude ?? 0.0),
                infoWindow: InfoWindow(
                    title: '${driver.driverUuid}',
                    snippet: 'Dhaka, Bangladesh'),
              ),
            );
          }
          );
        });
      } else if (response.findSuccessResponse!.isEmpty) {
        marker.clear();
      }
    });

    marker.add(Marker(
        markerId: MarkerId(storage.providerUuid.val),
        position: sourcePosition,
        infoWindow: InfoWindow(
            title: storage.providerUuid.val, snippet:"${place[0].street}, ""${place[0].subLocality}, ""${place[0].locality}")));


    update();
  }

  void getPlaceMark() async {
    List<Placemark> placemark = await placemarkFromCoordinates(
        currentLat.value, currentLng.value,
        localeIdentifier: 'en');
    place = placemark;
    print(place.first);
    print(place[0].locality);
    print(place[0].subLocality);
    print(place[0].street);
  }

  @override
  void onClose() {
    if (timer != null) {
      timer!.cancel();
    }
    super.onClose();
  }

}
