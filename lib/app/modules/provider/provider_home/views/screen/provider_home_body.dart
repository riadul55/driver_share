import 'package:driver_share/app/modules/provider/provider_home/controllers/provider_home_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import '../../../../../routes/app_pages.dart';
import '../../../../../values/utilities/storage_pref.dart';
import '../../../../global_components/views/global_button_widget.dart';
import '../../../../global_components/views/global_text_field_widget.dart';

class ProviderHomeBody extends StatelessWidget {
  final ProviderHomeController controller;

   ProviderHomeBody({Key? key, required this.controller})
      : super(key: key);

  late GoogleMapController googleMapController;
  CameraPosition initialPosition = const CameraPosition(
    zoom: 10,
    tilt: 0,
    bearing: 0,
    target: LatLng(52.2087498, -1.4616598),
  );
  @override
  Widget build(BuildContext context) {
    StoragePref storage = Get.find<StoragePref>();
    return Obx(() => GoogleMap(
      // polylines: controller.polylines,
      mapType: MapType.normal,
      markers: Set<Marker>.of(controller.marker),
      onMapCreated: controller.onMapCreated,
      // onMapCreated: (GoogleMapController ctr){
      //googleMapController=ctr;
      // },
      initialCameraPosition: initialPosition,
      circles: controller.circles,
    ));
  }
}
