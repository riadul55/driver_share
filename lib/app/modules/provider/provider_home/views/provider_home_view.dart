import 'package:driver_share/app/modules/global_components/views/global_bottom_sheet_container.dart';
import 'package:driver_share/app/modules/provider/provider_home/views/components/tab_call_driver_popup.dart';
import 'package:driver_share/app/modules/provider/provider_home/views/screen/provider_home_body.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../global_components/base_widget.dart';
import '../controllers/provider_home_controller.dart';
import 'components/mobile_call_driver_popup.dart';
import 'components/provider_navigation_drawer.dart';

class ProviderHomeView extends GetView<ProviderHomeController> {
  var scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    if(size.width>480 && size.width<=1280){
      //For Tab
    return BaseWidget(
      child: Scaffold(
        key: scaffoldKey,
        appBar: AppBar(
          title:  Text('Provider Home',style: TextStyle(color:Theme.of(context).colorScheme.onPrimary),),
          centerTitle: false,
            actions:  [
              TextButton(
                onPressed: (){
                  // showDialog(context: context, builder: (context){
                  //   return Dialog(
                  //     shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
                  //     elevation: 16,
                  //     child: TabCallDriverPopup(controller: controller,),
                  //   );
                  // });
                  scaffoldKey.currentState!.openEndDrawer();
                },
                child:  Text("Call Driver",style: TextStyle(fontSize: 20,color: Theme.of(context).colorScheme.onPrimary),
                ),
              ),
              const SizedBox(width: 10,)
            ]
        ),
        drawer: ProviderNavigationDrawer(controller: controller,),
        endDrawer: TabCallDriverPopup(controller: controller,),
        onEndDrawerChanged: (bool isOpen) {
          if (!isOpen) {
            // controller.address.clear();
          }
        },
        endDrawerEnableOpenDragGesture: false,
        body: ProviderHomeBody(controller: controller,),

        floatingActionButtonLocation:
        FloatingActionButtonLocation.miniStartFloat,
        floatingActionButton: FloatingActionButton(
          // isExtended: true,
          child:  Icon(
            Icons.location_searching,
            color: Theme.of(context).colorScheme.surface,
          ),
          backgroundColor: Theme.of(context).colorScheme.primaryContainer,
          onPressed: () {
            controller.showCurrentLocation();
          },
        ),
      ),
    );
    }else{
      //For Mobile
      return BaseWidget(
        child: Scaffold(
          appBar: AppBar(
              title:  Text('Provider Home',style: TextStyle(color: Theme.of(context).colorScheme.onPrimary),),
              centerTitle: false,
              actions:  [
                TextButton(
                  onPressed: (){
                    // showDialog(context: context, builder: (context){
                    //   return Dialog(
                    //     shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
                    //     elevation: 16,
                    //     child: FindDriverPopup(controller: controller,),
                    //   );
                    // });
                    callDriverForm(context);
                  },
                  child:  Text("Call Driver",style: TextStyle(fontSize: 20,color: Theme.of(context).colorScheme.onPrimary),
                  ),
                ),
                const SizedBox(width: 10,)
              ]
          ),
          drawer: ProviderNavigationDrawer(controller: controller,),
          body: ProviderHomeBody(controller: controller,),
          floatingActionButtonLocation:
          FloatingActionButtonLocation.miniStartFloat,
          floatingActionButton: FloatingActionButton(
            // isExtended: true,
            child:  Icon(
              Icons.location_searching,
              color: Theme.of(context).colorScheme.surface,
            ),
            backgroundColor: Theme.of(context).colorScheme.primaryContainer,
            onPressed: () {
              controller.showCurrentLocation();
            },
          ),
        ),
      );
    }
  }

  void callDriverForm(BuildContext context){
    globalBottomSheetContainer(
        context: context,
        child: MobileCallDriverPopup(controller: controller),
    );
  }
}
