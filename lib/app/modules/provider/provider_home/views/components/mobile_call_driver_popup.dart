import 'package:driver_share/app/modules/global_components/views/global_text_field_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_obx_widget.dart';
import '../../../../global_components/views/global_button_widget.dart';
import '../../../../global_components/views/global_drop_down_widget.dart';
import '../../../../global_components/views/global_input_field_widget.dart';
import '../../controllers/provider_home_controller.dart';

class MobileCallDriverPopup extends StatelessWidget {
  final ProviderHomeController controller;

  MobileCallDriverPopup({Key? key, required this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    double paddingSize=size.width-80;
    return SizedBox(
      width: size.width,
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(top: 25,bottom: 5,left: 40,right: 40),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              const SizedBox(
                height: 10,
              ),
              const Center(child: Text("Call Driver",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 22),)),
              const SizedBox(
                height: 15,
              ),
              const Text(
                "Phone (Optional)",
                style: TextStyle(fontSize: 14),
              ),
              const SizedBox(
                height: 10,
              ),
              Obx(
                () => GlobalTextFieldWidget(
                  hintText: 'Phone Number',
                  controller: controller.phoneController,
                  contentPadding: 12,
                  onChanged: controller.phone,
                  keyboardType: TextInputType.number,
                  errorText: controller.phoneError.value,
                  focusNode: controller.phoneFocus,
                  onFieldSubmit: (_) {
                    controller.postCodeFocus.requestFocus();
                  },
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              const Text(
                "Post Code",
                style: TextStyle(fontSize: 14),
              ),
              const SizedBox(
                height: 10,
              ),
              Autocomplete(
                optionsBuilder: (
                    TextEditingValue textValue,
                    ) async {
                  if (textValue.text.isEmpty) {
                    return const Iterable<String>.empty();
                  } else {
                    if (textValue.text.length >2) {
                      await controller.getPostCodes(textValue.text.toString());
                    }

                    return controller.autoCompleteData
                        .where((element) => element
                        .toLowerCase()
                        .contains(textValue.text.toLowerCase()))
                        .toList();
                  }
                },
                fieldViewBuilder:
                    (context, controlle, focusNode, onEditingComplete) {
                  return TextField(
                    controller: controlle,
                    focusNode: focusNode,
                    onChanged: (value) {
                      controller.postCode(value);
                    },
                    onEditingComplete: onEditingComplete,
                    decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5),
                        borderSide:  const BorderSide(
                          color: Colors.grey,

                        ),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5),
                        borderSide:  BorderSide(
                          width: 2,
                          color: Theme.of(context).colorScheme.primaryContainer,
                        ),
                      ),
                      border:const OutlineInputBorder(
                        // borderRadius: BorderRadius.circular(5),
                        // borderSide: BorderSide(
                        //     color: Theme.of(context).colorScheme.primary)
                      ),
                      hintText: "Post Code",
                    ),
                  );
                },
                onSelected: (data){
                  print(data);
                  controller.selectedPostCode.value=data.toString();
                  controller.getAddress(data.toString());
                },
                optionsViewBuilder: (context,  Function(String) onSelected, options){
                  return Align(alignment: Alignment.topLeft,
                    child: Material(
                      child: Container(
                        height: 300,
                        color: controller.colorLight,
                        width: paddingSize,
                        child: ListView.builder(
                            padding: EdgeInsets.all(10),
                            itemCount: options.length,
                            itemBuilder: (BuildContext context, int index){
                              final option=options.elementAt(index);
                              return GestureDetector(
                                onTap: (){
                                  onSelected(option.toString());
                                  FocusScope.of(context).unfocus();
                                  },
                                child: ListTile(
                                  title: Text(option.toString(),
                                  style: const TextStyle(color: Colors.black),),
                                ),
                              );
                            },
                        ),
                      ),
                    ),);
                },
              ),

              const SizedBox(
                height: 10,
              ),
              const Text(
                "Address",
                style: TextStyle(fontSize: 14),
              ),
              Obx(()=> GlobalDropDownContainer(
                  hintText: "Select Address",
                  items: controller.address.toList(),
                  onChange: (value) {
                    controller.selectedDropDown(value);
                    controller.selectedIndex.value=controller.address.indexOf(value);
                    controller.getHouseNumber(controller.address.indexOf(value));
                    // controller.getCustomerLatLon(controller.selectedDropDown(value));
                   // controller.houseNumber.value=controller.fullAddress[controller.address.indexOf(value)].buildingNumber.toString();
                  },
                  selectedValue: controller.selectedDropDown.value,

                ),
              ),
              const Text(
                "House Number",
                style: TextStyle(fontSize: 14),
              ),
              const SizedBox(
                height: 10,
              ),
              Obx(
                () => GlobalTextFieldWidget(
                  hintText: 'House Number',
                  controller: controller.houseNumberController,
                  contentPadding: 12,
                  onChanged: (value) => controller.houseNumber.value = value,
                  keyboardType: TextInputType.text,
                  errorText: controller.houseNumberError.value,
                  focusNode: controller.houseNumberFocus,
                  onFieldSubmit: (_) {
                    FocusScope.of(context).unfocus();
                  },
                ),
              ),
              /*const SizedBox(
                height: 10,
              ),
              const Text(
                "Street Name",
                style: TextStyle(fontSize: 14),
              ),
              const SizedBox(
                height: 10,
              ),
              Obx(
                () => GlobalTextFieldWidget(
                  contentPadding: 12,
                  hintText: 'Street Name',
                  onChanged: controller.streetName,
                  keyboardType: TextInputType.text,
                  errorText: controller.streetNameError.value,
                  focusNode: controller.streetNameFocus,
                  onFieldSubmit: (_) {
                    controller.townCityFocus.requestFocus();
                  },
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              const Text(
                "Town/City",
                style: TextStyle(fontSize: 14),
              ),
              const SizedBox(
                height: 10,
              ),
              Obx(
                () => GlobalTextFieldWidget(
                  hintText: 'Town/City',
                  contentPadding: 12,
                  onChanged: controller.townCity,
                  keyboardType: TextInputType.text,
                  errorText: controller.townCityError.value,
                  focusNode: controller.townCityFocus,
                  onFieldSubmit: (_) {
                    controller.radiusFocus.requestFocus();
                  },
                ),
              ),*/
              const SizedBox(
                height: 10,
              ),Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: size.width/3,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          "Street Name",
                          style: TextStyle(fontSize: 14),
                        ),

                        const SizedBox(
                          height: 10,
                        ),
                        Obx(
                              () => GlobalTextFieldWidget(
                                contentPadding: 12,
                                controller: controller.streetNameController,
                                hintText: 'Street Name',
                                isEnabled: false,
                                onChanged: (value) =>controller.streetName.value= value,
                                keyboardType: TextInputType.text,
                                errorText: controller.streetNameError.value,
                                focusNode: controller.streetNameFocus,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    width: size.width/3,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          "Town/City",
                          style: TextStyle(fontSize: 14),
                        ),

                        const SizedBox(
                          height: 10,
                        ),
                        GlobalTextFieldWidget(
                          hintText: 'Town/City',
                          controller: controller.townCityController,
                          contentPadding: 12,
                          isEnabled: false,
                          onChanged: (value) =>controller.townCity.value= value,
                          keyboardType: TextInputType.text,
                          errorText: controller.townCityError.value,
                          focusNode: controller.townCityFocus,
                          ),
                      ],
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              /*const Text(
                "Radius",
                style: TextStyle(fontSize: 14),
              ),
              const SizedBox(
                height: 10,
              ),
              Obx(
                () => GlobalTextFieldWidget(
                  hintText: 'Radius (Meter)',
                  contentPadding: 12,
                  onChanged: controller.inputRadius,
                  defaultText: controller.inputRadius.value,
                  errorText: controller.radiusError.value,
                  keyboardType: TextInputType.number,
                  focusNode: controller.radiusFocus,
                  onFieldSubmit: (_) {
                    FocusScope.of(context).unfocus();
                    controller.checkRadius(context);
                  },
                ),
              ),
              const SizedBox(
                height: 20,
              ),*/
              GlobalButtonWidget(
                text: "DriverLocation",
                pres: () {
                  controller.checkRadius(context);
                },
              ),
              const SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      ),
    );
  }
}