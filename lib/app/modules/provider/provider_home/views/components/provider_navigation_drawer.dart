import 'package:driver_share/app/modules/provider/provider_home/controllers/provider_home_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../routes/app_pages.dart';
import '../../../../../values/utilities/storage_pref.dart';
import '../../../provider_setting/views/provider_setting_view.dart';



class ProviderNavigationDrawer extends StatelessWidget {
  final ProviderHomeController controller;
  ProviderNavigationDrawer({Key? key, required this.controller}) : super(key: key);

  StoragePref storage = Get.find<StoragePref>();
  @override
  Widget build(BuildContext context) => Drawer(
    child: SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          buildHeader(context),
          buildMenuItems(context),
        ],
      ),
    ),
  );

  buildHeader(BuildContext context) => Container(
    color: Theme.of(context).colorScheme.primary,
    padding: EdgeInsets.only(
      top: MediaQuery.of(context).padding.top,
    ),
    child: Column(
      children: [
        const SizedBox(
          height: 8,
        ),
        const CircleAvatar(
          radius: 52,
          backgroundImage: NetworkImage(
              'https://cdn.wisden.com/wp-content/uploads/2017/11/GettyImages-688893396-e1520011475170.jpg'),
        ),
        const SizedBox(
          height: 12,
        ),
        Text(
          storage.loginID.val,
          style:  TextStyle(fontSize: 20, color: Theme.of(context).colorScheme.onPrimary),
        ),
        const SizedBox(
          height: 12,
        ),
      ],
    ),
  );

  buildMenuItems(BuildContext context) => Container(
    padding: const EdgeInsets.all(24),
    child: Wrap(
      runSpacing: 8,
      children: [
        ListTile(
          leading:  Icon(Icons.home_filled,color:Theme.of(context).colorScheme.primaryContainer ),
          title:  Text("Home",style: TextStyle(color: Theme.of(context).colorScheme.onPrimary),),
          onTap: () {
            //close navigation drawer before
            Navigator.pop(context);
          },
        ),
        ListTile(
          leading:  Icon(Icons.calendar_today_rounded,color:Theme.of(context).colorScheme.primaryContainer ),
          title:  Text("Schedule",style: TextStyle(color: Theme.of(context).colorScheme.onPrimary)),
          onTap: () {
            //close navigation drawer before
            Navigator.pop(context);
            Get.toNamed(Routes.SCHEDULE);
          },
        ),
        const Divider(
          color: Colors.black54,
        ),
        ListTile(
          leading:  Icon(Icons.settings,color:Theme.of(context).colorScheme.primaryContainer ),
          title:  Text("Settings",style: TextStyle(color: Theme.of(context).colorScheme.onPrimary)),
          onTap: () {//close navigation drawer before
            Navigator.pop(context);
            Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => ProviderSettingView(),
            ));},
        ),
        ListTile(
          leading:  Icon(Icons.notifications,color:Theme.of(context).colorScheme.primaryContainer ),
          title:  Text("Notification",style: TextStyle(color: Theme.of(context).colorScheme.onPrimary)),
          onTap: () {},
        ),
        ListTile(
          leading:  Icon(Icons.info,color:Theme.of(context).colorScheme.primaryContainer ),
          title:  Text("About",style: TextStyle(color: Theme.of(context).colorScheme.onPrimary)),
          onTap: () {},
        ),
        ListTile(
          leading:  Icon(Icons.logout_sharp,color:Theme.of(context).colorScheme.primaryContainer ,),
          title:  Text("Log out",style: TextStyle(color: Theme.of(context).colorScheme.onPrimary)),
          onTap: () {controller.signOut();},
        ),
      ],
    ),
  );
}
