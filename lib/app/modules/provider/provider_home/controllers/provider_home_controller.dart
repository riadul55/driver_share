import 'dart:async';
import 'dart:convert';
import 'dart:math';
import 'package:driver_share/app/data/remote_db/models/find_driver_model/find_driver_response.dart';
import 'package:driver_share/app/data/remote_db/models/location_models/order_notification_resopnse.dart';
import 'package:driver_share/app/data/remote_db/provider/find_driver_provider.dart';
import 'package:driver_share/app/data/remote_db/provider/order_notification_provider.dart';
import 'package:driver_share/app/data/remote_db/provider/post_code_porvider.dart';
import 'package:driver_share/app/modules/global_components/log_info.dart';
import 'package:driver_share/app/values/themes/app_themes.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geocoding/geocoding.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';
import '../../../../data/remote_db/models/location_models/position_model.dart';
import '../../../../data/remote_db/models/post_code_model/post_code_address_response.dart';
import '../../../../data/remote_db/models/post_code_model/post_code_response.dart';
import '../../../../routes/app_pages.dart';
import '../../../../values/utilities/storage_pref.dart';
import '../../../global_components/controllers/loading_controller.dart';
import '../../../global_components/controllers/location_controller.dart';
import '../../../global_components/snack_bar.util.dart';

import 'package:socket_io_client/socket_io_client.dart' as IO;

class ProviderHomeController extends GetxController {
  StoragePref storage = Get.find<StoragePref>();
  final _loadingController = Get.find<LoadingController>();
  late GoogleMapController googleMapController;
  final Completer<GoogleMapController> _completerController = Completer();
  final LocationController locationController = Get.find<LocationController>();
  late FindDriverProvider findDriverProvider;
  late PostCodeProvider postCodeProvider;
  late OrderNotificationProvider orderNotificationProvider;

  var currentPosition = Position().obs;
  var currentLat = 51.89282320966875;
  var currentLng = -0.4302055153683773;
  LatLng providerPosition =const LatLng(51.89282320966875, -0.4302055153683773);
  Timer? timer;
  late Set<Circle> circles = <Circle>{}.obs;

  Color colorDark = onPrimary;
  Color colorLight = onPrimary.withOpacity(.1);
  List<Placemark> place = [];

  List<Marker> marker = <Marker>[].obs;
  late BitmapDescriptor mapMarker;
  List<FindSuccessResponse> drivers = <FindSuccessResponse>[].obs;

  //input controllers
  var houseNumberController = TextEditingController();
  var streetNameController = TextEditingController();
  var townCityController = TextEditingController();
  var phoneController = TextEditingController();

  late IO.Socket socket;
  late final radius = storage.defaultRadius.val.obs;
  @override
  void onInit() {
    ever(locationController.currentPosition, locationProcess);
    ever<String>(phone, validatePhone);
    debounce<String>(postCode, validatePostCode);
    ever<String>(houseNumber, validateHouseNumber);
    ever<String>(streetName, validateStreetName);
    ever<String>(townCity, validateTownCity);
    findDriverProvider = FindDriverProvider();
    postCodeProvider = PostCodeProvider();
    orderNotificationProvider = OrderNotificationProvider();
    timer = Timer.periodic(
        const Duration(seconds: 10), (Timer t) => updatePosition());
    // getPlaceMark();
    circles = {
      Circle(
        circleId: CircleId(storage.providerUuid.val),
        center: providerPosition,
        strokeColor: colorDark,
        strokeWidth: 1,
        fillColor: colorLight,
        radius: double.parse(storage.defaultRadius.val),
      )
    };

    socketConnection();
    super.onInit();
  }

  double getZoomLevel(double radius) {
    double zoomLevel = 11;
    if (radius > 0) {
      double radiusElevated = radius + radius / 2;
      double scale = radiusElevated / 500;
      zoomLevel = 16.35 - log(scale) / log(2);
    }
    // zoomLevel = num.parse(zoomLevel.toStringAsFixed(2));
    zoomLevel = double.parse(zoomLevel.toStringAsFixed(2));
    return zoomLevel;
  }

  void onMapCreated(GoogleMapController controller) {
    _completerController.complete(controller);
    logInfo("map creating....");
    updatePosition();
  }

  void showCurrentLocation() async {
    var sourcePosition = providerPosition;
    final GoogleMapController ctr = await _completerController.future;

    CameraPosition cameraPosition = CameraPosition(
        zoom: getZoomLevel(double.parse(radius.value)),
        tilt: 0,
        bearing: 0,
        target: sourcePosition);
    ctr.animateCamera(CameraUpdate.newCameraPosition(cameraPosition));
  }

  void updatePosition() async {
    logInfo("provider position is updating.....");
    // currentLat.value = providerHomeController.currentLat.value;
    // currentLng.value = providerHomeController.currentLng.value;
    var sourcePosition = LatLng(currentLat, currentLng);
    final GoogleMapController ctr = await _completerController.future;

    CameraPosition cameraPosition = CameraPosition(
        zoom: getZoomLevel(double.parse(radius.value)),
        tilt: 0,
        bearing: 0,
        target: sourcePosition);
    ctr.animateCamera(CameraUpdate.newCameraPosition(cameraPosition));
    // marker.removeWhere((item) => item.markerId.value == storage.providerUuid.val);
    circles = {
      Circle(
        circleId: CircleId(storage.providerUuid.val),
        center: providerPosition,
        strokeColor: colorDark,
        strokeWidth: 1,
        fillColor: colorLight,
        radius: double.parse(storage.defaultRadius.val),
      )
    };
    await findDriverProvider
        .getDriverLocation(
            currentLat, currentLng, double.parse(radius.value))
        .then((FindDriverResponse response) {
      if (response != null &&
          response.findSuccessResponse != null &&
          response.findSuccessResponse!.isNotEmpty) {
        marker.clear();
        drivers = response.findSuccessResponse!.toList();
        BitmapDescriptor.fromAssetImage(
                const ImageConfiguration(), 'assets/icons/car2.png')
            .then((icon) {
          drivers.forEach((FindSuccessResponse driver) {
            marker.add(
              Marker(
                markerId: MarkerId('${driver.driverUuid}'),
                icon: icon,
                rotation: driver.heading!,
                position:
                    LatLng(driver.latitude ?? 0.0, driver.longitude ?? 0.0),
                infoWindow: InfoWindow(
                    title: '${driver.driverUuid}',
                    snippet: 'Driver'),
              ),
            );
          });
        });
      } else if (response.findSuccessResponse!.isEmpty) {
        marker.clear();
      }
    });
    marker.add(Marker(
        markerId: MarkerId(storage.providerUuid.val),
        position: sourcePosition,
        infoWindow: InfoWindow(
          title: storage.providerUuid.val,
          // snippet: "${place[0].street}, "
          //     "${place[0].subLocality}, "
          //     "${place[0].locality}"
        )));

    update();
  }

  /*void getPlaceMark() async {
    List<Placemark> placemark = await placemarkFromCoordinates(
        currentLat.value, currentLng.value,
        localeIdentifier: 'en');
    place = placemark;
    print(place.first);
    print(place[0].locality);
    print(place[0].subLocality);
    print(place[0].street);
  }*/

  locationProcess(Position position) {
    currentPosition(position);
    // currentLat.value = position.latitude!;
    // currentLng.value = position.longitude!;

    print("Position  foreground==> ${position.latitude}, "
        "${position.longitude}");
  }

  void signOut() {
    storage.isLoggedIn.val = false;
    storage.isDriverLoggedIn.val = false;
    storage.sessionId.val = "";
    storage.driverUuid.val = "";
    storage.providerUuid.val = "";
    storage.defaultRadius.val = '2000';
    storage.loginID.val="";

    Get.offAllNamed(Routes.LOGIN);
  }

  late List<String> driversUidList = <String>[].obs;
  List<FindSuccessResponse> car = <FindSuccessResponse>[].obs;

  Future<void> getDriverLocation() async {
    car.clear();
    driversUidList.clear();
    _loadingController.isLoading = true;
    double radix = double.parse(radius.value);
    storage.defaultRadius.val = radius.value.toString();
    String currentDateTime = DateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").format(DateTime.now()).toString();
    await findDriverProvider
        .getDriverLocation( currentLng,currentLat, radix)
        .then((FindDriverResponse response) {
      if (response != null &&
          response.findSuccessResponse != null &&
          response.findSuccessResponse!.isNotEmpty) {
        car = response.findSuccessResponse!.toList();
        car.forEach((FindSuccessResponse driver) {
          driversUidList.add(driver.driverUuid.toString());
        });
        logInfo(driversUidList.toList().toString());

        updatePosition();
        _loadingController.isLoading = false;
      } else if (response.findSuccessResponse!.isEmpty) {
        updatePosition();
        _loadingController.isLoading = false;
        SnackbarUtil.showError(message: "No driver found!");
      } else {
        print(response.findErrorResponse!.errorDescription);
        _loadingController.isLoading = false;
        SnackbarUtil.showError(message: "Something went wrong!");
      }
      _loadingController.isLoading = false;
    });
  }

  @override
  void onClose() {
    if (timer != null) {
      timer!.cancel();
    }
    super.onClose();
  }


  /*late final inputRadius = ''.obs;
  final radiusError = RxnString();
  final radiusFocus = FocusNode();

  void validateRadius(String val) {
    // RegExp regex = RegExp(r'^[0-9]*$');
    RegExp regex = RegExp(r'(^\d*\.?\d*)$');
    if (val.isEmpty) {
      radiusError.value = 'Enter Radius';
    } else if (val.isNotEmpty) {
      if (!regex.hasMatch(val)) {
        radiusError.value = "Invalid number!";
      } else if (regex.hasMatch(val)) {
        if (double.parse(val.toString()) > 100000) {
          radiusError.value = "Radius need to be below 100000 meter";
        } else {
          radiusError.value = null;
        }
      }
    }
  }*/

  late final phone = "".obs;
  final phoneError = RxnString();
  final phoneFocus = FocusNode();

  void validatePhone(String val) {
    // RegExp regex = RegExp(r'^[0-9]*$');
    RegExp regex = RegExp(r'(^\d*\.?\d*)$');
    if (val.isEmpty) {
      phoneError.value = null;
    } else {
      if (!regex.hasMatch(val)) {
        phoneError.value = "Invalid Phone number!";
      } else {
        phoneError.value = null;
      }
    }
  }

  late final postCode = "".obs;
  final postCodeError = RxnString();
  final postCodeFocus = FocusNode();

  void validatePostCode(String val) {
    if (val.isEmpty) {
      postCodeError.value = 'Enter post code';
    } else {
      postCodeError.value = null;
    }
  }

  final houseNumber = "".obs;
  final houseNumberError = RxnString();
  final houseNumberFocus = FocusNode();

  void validateHouseNumber(String val) {
    if (val.isEmpty) {
      houseNumberError.value = 'Enter house number';
    } else {
      houseNumberError.value = null;
    }
  }

  late var streetName = "".obs;
  final streetNameError = RxnString();
  final streetNameFocus = FocusNode();

  void validateStreetName(String val) {
    if (val.isEmpty) {
      streetNameError.value = 'Enter street name';
    } else {
      houseNumberError.value = null;
    }
  }

  final townCity = "".obs;
  final townCityError = RxnString();
  final townCityFocus = FocusNode();

  void validateTownCity(String val) {
    if (val.isEmpty) {
      townCityError.value = 'Enter town/city';
    } else {
      townCityError.value = null;
    }
  }

  var customerAddress="".obs;
  void checkRadius(BuildContext context) {
    if (postCode.value.isEmpty || postCodeError.value != null) {
      SnackbarUtil.showWarning(message: "please enter post code");
    } else if (houseNumber.value.isEmpty || houseNumberError.value != null) {
      SnackbarUtil.showWarning(message: "please enter house number");
    } /*else if (inputRadius.value.isEmpty || radiusError.value != null) {
      SnackbarUtil.showWarning(message: "please enter radius");
    }*/ else {
      // radius.value = inputRadius.value;
      phoneController.text=phone.value;
      Navigator.of(context).pop();
      customerAddress.value="${houseNumber.value}, "
          "${streetName.value}, ${townCity.value}";
      logSuccess(customerAddress.value);
      getCustomerLatLon(customerAddress.value);
    }
  }

  void checkRadiusForTab(BuildContext context) {
    if (postCode.value.isEmpty || postCodeError.value != null) {
      SnackbarUtil.showWarning(message: "please enter post code");
    } else if (houseNumber.value.isEmpty || houseNumberError.value != null) {
      SnackbarUtil.showWarning(message: "please enter house number");
    } /*else if (inputRadius.value.isEmpty || radiusError.value != null) {
      SnackbarUtil.showWarning(message: "please enter radius");
    }*/ else {
      // radius.value = inputRadius.value;

      phoneController.text=phone.value;
      customerAddress.value="${houseNumber.value}, "
          "${streetName.value}, ${townCity.value}";
      getCustomerLatLon(customerAddress.value);
    }
  }

  late List<String> autoCompleteData = <String>[].obs;
  late var selectedPostCode = "".obs;

  Future<void> getPostCodes(String pCode) async {
    await postCodeProvider.getPostCode(pCode).then((PostCodeResponse response) {
      if (response.successResponse != null) {
        autoCompleteData.clear();
        autoCompleteData.addAll(response.successResponse.toList());
        update();
      }
    });
  }

  late List<AddressSuccessResponse> fullAddress =
      <AddressSuccessResponse>[].obs;

  final selectedDropDown = "".obs;
  late List<String> address = <String>[].obs;
  late var selectedIndex = 0.obs;

  Future<void> getAddress(String code) async {
    _loadingController.isLoading = true;
    await postCodeProvider
        .getAddress(code)
        .then((PostCodeAddressResponse response) {
      if (response.addressSuccessResponse != null) {
        fullAddress.clear();
        selectedDropDown.value = "";
        address.clear();
        fullAddress.addAll(response.addressSuccessResponse!);
        for (int i = 0; i < fullAddress.length; i++) {
          if (fullAddress[i].buildingNumber.toString() == " ") {
            address.add("${fullAddress[i].buildingName.toString()}, "
                "${fullAddress[i].thoroughfare.toString()}, "
                "${fullAddress[i].deliveryPointSuffix.toString()}, "
                "${fullAddress[i].town.toString()}");
          } else {
            address.add("${fullAddress[i].buildingNumber.toString()}, "
                "${fullAddress[i].thoroughfare.toString()}, "
                "${fullAddress[i].deliveryPointSuffix.toString()}, "
                "${fullAddress[i].town.toString()}");
          }
        }
        selectedDropDown.value = address[0];
        getHouseNumber(0);
        // getCustomerLatLon(selectedDropDown.value.toString());
        update();
        _loadingController.isLoading = false;
      } else {
        _loadingController.isLoading = false;
      }
    });
  }

  List<Location> getLatLng = <Location>[].obs;

  getCustomerLatLon(String address) async {
    getLatLng.clear();
    getLatLng = await locationFromAddress(address);
    print(getLatLng);
    // SnackbarUtil.showSuccess(message: "lat==> ${getLatLng[0].latitude} lon==> ${getLatLng[0].longitude}");
    // getDriverLocation();
    sendNotification(address,getLatLng[0].latitude,getLatLng[0].longitude);
  }

  Future<void> sendNotification(String customerAddress, double customerLat,double customerLng)async{
    logInfo("${storage.providerUuid.val} ${storage.loginID.val}  Imran Hossain ,${phone.value}"
        " ${houseNumber.value} ${streetName.value} ${townCity.value} "
        "${selectedPostCode.value} ${customerLat} ${customerLng}");
    await orderNotificationProvider.setOrder(storage.providerUuid.val,
        storage.loginID.val,"Imran Hossain",phone.value,houseNumber.value,streetName.value,
        townCity.value,selectedPostCode.value,customerLat,customerLng).
    then((OrderNotification response) {
        logSuccess("Send Notification Response message =====> ${response.message}");
    });
  }

  getHouseNumber(int index) {
    if (fullAddress[index].buildingNumber.toString() == " ") {
      houseNumber.value = fullAddress[index].buildingName.toString();
    } else {
      houseNumber.value = fullAddress[index].buildingNumber.toString();
    }
    streetName.value = fullAddress[index].thoroughfare.toString();
    townCity.value = fullAddress[index].town.toString();
    houseNumberController.text = houseNumber.value;
    streetNameController.text = streetName.value;
    townCityController.text = townCity.value;
    update();
  }

  ///socket connection
  void socketConnection() {
    logInfo("connecting socket....");
    socket = IO.io('http://192.168.0.108:4000', <String, dynamic>{
      'transports': ['websocket'],
      'autoConnect': false,
    });
    socket.connect();
    socket.onConnect((_) {
      logSuccess('socket is connected!');
    });

    socket.onDisconnect((_) {
      logError("socket is disconnected!");
    });

    socket.on('ACCEPTED', (data) {
      print(data);
    });
  }
}
