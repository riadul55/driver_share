
// Blue text
void logInfo(Object msg) {
  print('\x1B[34m$msg\x1B[0m');
}

// Green text
void logSuccess(String msg) {
  print('\x1B[32m$msg\x1B[0m');
}

// Yellow text
void logWarning(String msg) {
  print('\x1B[33m$msg\x1B[0m');
}

// Red text
void logError(Object msg) {
  print('\x1B[31m$msg\x1B[0m');
}

// static const String red = '\u001b[31m';
// static const String green = '\u001b[32m';
// static const String blue = '\u001b[34m';
// static const String cyan = '\u001b[36m';
// static const String magenta = '\u001b[35m';
// static const String yellow = '\u001b[33m';
// static const String grey = '\u001b[90m';