import 'package:flutter/material.dart';

globalBottomSheetContainer({
  context,
  padding,
  isScroll,
  child,
}) =>
    showModalBottomSheet(
      context: context,
      isScrollControlled: isScroll ?? true,
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(20.0))),
      builder: (context) => Padding(
        padding: MediaQuery.of(context).viewInsets,
        child: Padding(
          padding: EdgeInsets.all(padding ?? 5),
          child: child,
        ),
      ),
    );
