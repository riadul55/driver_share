import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../values/utilities/constants.dart';

class DropDownList extends StatelessWidget {
  final String hintText;
  final List<String> items;
  final double? width;
  final Color? isNotValid;
  final onChange;
  final String? selectedValue;

  const DropDownList({
    Key? key,
    required this.hintText,
    required this.items,
    this.width,
    this.isNotValid,
    required this.onChange,
    this.selectedValue,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = Get.size;
    return Container(
        margin: EdgeInsets.symmetric(vertical: Constants.defaultPadding / 2),
        width: width != null ? (size.width * this.width!) : Get.width,
        padding: EdgeInsets.symmetric(horizontal: Constants.defaultPadding / 2),
        decoration: BoxDecoration(
            border: Border.all(
              color: isNotValid ??
                  Theme.of(context).colorScheme.onSecondary.withOpacity(0.5),
            ),
            color: Theme.of(context).colorScheme.background,
            borderRadius: BorderRadius.circular(5)),
        child: DropdownButton<String>(
          value: selectedValue != null && selectedValue!.isNotEmpty
              ? selectedValue
              : null,
          isExpanded: true,
          underline: SizedBox(),
          hint: Text(
            hintText,
            style: TextStyle(
              color: Theme.of(context).colorScheme.onSecondary.withOpacity(0.5),
            ),
          ),
          style: TextStyle(color: Theme.of(context).colorScheme.onBackground),
          items: items.map((String value) {
            return DropdownMenuItem<String>(
              value: value,
              child: new Text(value),
            );
          }).toList(),
          onChanged: onChange,
        ));
  }
}
