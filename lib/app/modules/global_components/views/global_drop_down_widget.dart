import 'package:flutter/material.dart';
import 'package:get/get.dart';

class GlobalDropDownContainer extends StatelessWidget {
  final String hintText;
  final List<String> items;
  final double? width;
  final Color? isNotValid;
  final onChange;
  final String? selectedValue;

  const GlobalDropDownContainer({
    Key? key,
    required this.hintText,
    required this.items,
    this.width,
    this.isNotValid,
    required this.onChange,
    this.selectedValue,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = Get.size;
    return Container(
        margin: const EdgeInsets.symmetric(vertical: 10),
        width: width != null ? (size.width * width!) : Get.width,
        padding: const EdgeInsets.symmetric(horizontal: 10),
        decoration: BoxDecoration(
            border: Border.all(
              color: isNotValid ??
                  Theme.of(context).colorScheme.onSecondary.withOpacity(0.5),
            ),
            color: Theme.of(context).colorScheme.background,
            borderRadius: BorderRadius.circular(5)),
        child: DropdownButton<String>(
          value: selectedValue != null && selectedValue!.isNotEmpty
              ? selectedValue
              : null,
          isExpanded: true,
          underline: SizedBox(),
          hint: Text(
            hintText,
            style: const TextStyle(
              color: Colors.black,
            ),
          ),
          style: const TextStyle(
            color: Colors.black,
          ),
          items: items.map((String value) {
            return DropdownMenuItem<String>(
              value: value != null && value.isNotEmpty ? value : null,
              child: Text(value),
            );
          }).toList(),
          onChanged: onChange,
        ));
  }
}
