import 'package:flutter/material.dart';
import 'package:get/get.dart';

class GlobalInputFieldWidget extends StatelessWidget {
  final String hintText;
  final controller;
  final String? errorText;
  final TextInputType? keyboardType;
  final Color? isNotValid;
  final FocusNode? focusNode;
  final isEnabled;
  final double? contentPadding;
  final ValueChanged<String> onChanged;
  final ValueChanged<String>? onFieldSubmit;

  const GlobalInputFieldWidget(
      {Key? key,
      required this.hintText,
      this.controller,
      this.keyboardType,
      this.isNotValid,
      this.isEnabled,
      required this.onChanged,
        this.focusNode, this.errorText, this.onFieldSubmit,
        this.contentPadding})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = Get.size;
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 5),
      width: size.width,
      padding: const EdgeInsets.symmetric(horizontal: 20),
      decoration: BoxDecoration(
          border: Border.all(
            color: Theme.of(context).colorScheme.primary,
          ),
          color: Theme.of(context).colorScheme.background,
          borderRadius: BorderRadius.circular(5)),
      child: TextField(
        controller: controller,
        focusNode: focusNode ?? FocusNode(),
        enabled: isEnabled ?? true,
        keyboardType: keyboardType ?? TextInputType.text,
        onChanged: onChanged,
        onSubmitted: onFieldSubmit,
        style: TextStyle(color: Theme.of(context).colorScheme.onBackground),
        decoration: InputDecoration(
          contentPadding: EdgeInsets.symmetric(vertical: contentPadding ?? 12,),
          hintText: hintText,
          errorText: errorText,
          hintStyle: TextStyle(
            color: Theme.of(context).colorScheme.onSecondary.withOpacity(0.5),
          ),
          border: InputBorder.none,
        ),
      ),
    );
  }
}
