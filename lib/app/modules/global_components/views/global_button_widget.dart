import 'package:flutter/material.dart';
import 'package:get/get.dart';

class GlobalButtonWidget extends StatelessWidget {
  final String text;
  final Color? bgColor;
  final Color? textColor;
  final double? fontSize;
  final Function()? pres;

  const GlobalButtonWidget(
      {Key? key,
      required this.text,
      this.bgColor,
      this.textColor,
      this.fontSize,
      this.pres})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: pres,
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.all(bgColor?? Theme.of(context).colorScheme.primaryContainer),
      ),
      child: SizedBox(
        height: 60,
        width: double.infinity,
        child: Center(
          child: Text(
            text,
            style: TextStyle(
              color: textColor ?? Theme.of(context).colorScheme.surface,
              fontSize: fontSize ?? 25,
            ),
          ),
        ),
      ),
    );
  }
}
