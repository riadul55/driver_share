import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class GlobalTextFieldWidget extends StatelessWidget {
  final TextEditingController? controller;
  final String? hintText;
  final String? defaultText;
  final String? errorText;
  final Color? hintColor;
  final bool? isEnabled;
  final ValueChanged<String> onChanged;
  final FocusNode? focusNode;
  final double? contentPadding;
  final ValueChanged<String>? onFieldSubmit;
  final TextInputType? keyboardType;

  const GlobalTextFieldWidget({
    Key? key,
    this.controller,
    this.hintText,
    required this.onChanged,
    this.hintColor,
    this.errorText,
    this.focusNode,
    this.onFieldSubmit,
    this.defaultText,
    this.keyboardType, this.contentPadding, this.isEnabled,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      enabled: isEnabled ?? true,
      onChanged: onChanged,
      textAlign: TextAlign.center,
      initialValue: defaultText,
      focusNode: focusNode ?? FocusNode(),
      textInputAction: TextInputAction.next,
      keyboardType: keyboardType ?? TextInputType.text,
      onFieldSubmitted: onFieldSubmit,
      decoration: InputDecoration(
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5),
      borderSide:  const BorderSide(
        color: Colors.grey,

      ),
    ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5),
          borderSide:  BorderSide(
            width: 2,
            color: Theme.of(context).colorScheme.primaryContainer,
          ),
        ),

        border: const OutlineInputBorder(),
        contentPadding: EdgeInsets.symmetric(vertical: contentPadding ?? 20,),
        errorText: errorText,
        hintText: hintText,
        hintStyle: Get.textTheme.headline6
            ?.copyWith(color: hintColor ?? Theme.of(context).colorScheme.onPrimary),
      ),
    );
  }
}
