import 'package:flutter/material.dart';
import 'package:get/get.dart';

class OrDevider extends StatelessWidget {
  final double lineWidth;
   const OrDevider({Key? key, required this.lineWidth}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size =MediaQuery.of(context).size;
    return SizedBox(
      width: lineWidth,
      child: Row(children: [
        buildDivider(context),
        Padding(
          padding:  const EdgeInsets.symmetric(horizontal: 10),
          child: Text("Or",style: TextStyle(color: Theme.of(context).colorScheme.onPrimary,),),
        ),
        buildDivider(context),
      ],),
    );
  }

  buildDivider(BuildContext context) {
    return Expanded(child: Divider(color: Theme.of(context).colorScheme.onPrimary,
    height: 2,));
  }
}
