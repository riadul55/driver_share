import 'package:flutter/material.dart';
import 'package:get/get.dart';

class GlobalPasswordFieldWidget extends StatelessWidget {
  final String hintText;
  final String? errorText;
  final Color? hintColor;
  final FocusNode? focusNode;
  final ValueChanged<String> onChange;
  final ValueChanged<String>? onFieldSubmit;

  const GlobalPasswordFieldWidget(
      {Key? key,
      required this.hintText,
      required this.onChange,
      this.hintColor,
      this.errorText,
      this.onFieldSubmit, this.focusNode})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      onChanged: onChange,
      textAlign: TextAlign.center,
      focusNode: focusNode ?? FocusNode(),
      obscureText: true,
      textInputAction: TextInputAction.done,
      onFieldSubmitted: onFieldSubmit,
      decoration: InputDecoration(
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5),
          borderSide:  const BorderSide(
            color: Colors.grey,

          ),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5),
          borderSide:  BorderSide(
            width: 2.0,
            color: Theme.of(context).colorScheme.primaryContainer,
          ),
        ),
        border: const OutlineInputBorder(),
        errorText: errorText,
        hintText: hintText,
        hintStyle: Get.textTheme.headline6
            ?.copyWith(color: hintColor ?? Theme.of(context).colorScheme.onPrimary),
      ),
    );
  }
}
