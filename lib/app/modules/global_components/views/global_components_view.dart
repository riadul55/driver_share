import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/global_components_controller.dart';

class GlobalComponentsView extends GetView<GlobalComponentsController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('GlobalComponentsView'),
        centerTitle: true,
      ),
      body: Center(
        child: Text(
          'GlobalComponentsView is working',
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}
