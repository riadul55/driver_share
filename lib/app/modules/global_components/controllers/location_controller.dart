import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:location/location.dart';

import '../../../data/remote_db/models/location_models/get_location_response.dart';
import '../../../data/remote_db/models/location_models/location_response.dart';
import '../../../data/remote_db/models/location_models/position_model.dart';
import '../../../data/remote_db/provider/location_provider.dart';
import '../../../values/utilities/storage_pref.dart';
import '../log_info.dart';

class LocationController extends GetxService {

  Location location = Location();

  LocationProvider locationProvider = LocationProvider();
  late bool _serviceEnabled;
  late PermissionStatus _permissionGranted;
  late LocationData _locationData;

  var currentPosition = Position().obs;

  @override
  void onInit() async {
    super.onInit();
    locationProvider=LocationProvider();
    checkPermission();
    location.enableBackgroundMode(enable: true);
    location.onLocationChanged.listen((LocationData currentLocation) {
      currentPosition(Position(latitude: currentLocation.latitude,
          longitude: currentLocation.longitude,
          heading: currentLocation.heading));
      var storage = Get.put(StoragePref());
      var timestamp = DateTime.now().millisecondsSinceEpoch;
      var diff = DateTime.fromMillisecondsSinceEpoch(timestamp - storage.interval.val);

      if (diff.second > 10) {
        storage.interval.val = timestamp;

        if (storage.isLoggedIn.val && storage.isDriverLoggedIn.val) {
          print("Position from background==> ${currentLocation.latitude}, "
              "${currentLocation.longitude}");
          // Position position1 = await _determinePosition();
          // print(position1.latitude);
          // print(position1.longitude);
          // print(storage.sessionId.val);
          // print(storage.driverUuid.val);
          print("Driver uID==> ${storage.driverUuid.val}");
          print("sID==> ${storage.sessionId.val}");

          String currentDateTime = DateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").format(DateTime.now()).toString();


          locationProvider.sendLocation(storage.driverUuid.val, currentDateTime,
              "${currentLocation.latitude}", "${currentLocation.longitude}",
               // -0.42808804533143857,51.901376872016364,
              "${currentLocation.heading}", "${currentLocation.speed}")
              .then((LocationResponse response) {
            if (response.success) {
              print("Send Location Response ==> ${response.message}");
              logInfo("time==> $currentDateTime");
            } else {
              print(response.message);
            }
          });
        }
      }
    });
  }

  isLocationEnabled() async {
    return await location.serviceEnabled();
  }

  requestLocation() async {
    return await location.requestService();
  }

  checkPermission()async{
    try {
      _serviceEnabled = await isLocationEnabled();
      if (!_serviceEnabled) {
        _serviceEnabled = await requestLocation();
        if (!_serviceEnabled) {
          return false;
        }
      }

      _permissionGranted = await location.hasPermission();
      if (_permissionGranted == PermissionStatus.denied) {
        _permissionGranted = await location.requestPermission();
        if (_permissionGranted != PermissionStatus.granted) {
          return false;
        }
      }

      return true;
    } catch (e) {
      print(e.toString());
      return false;
    }
  }

  Future<void> getLocation() async {
    await locationProvider.getLocationInfo().then((LocationRes response) {
      if (response.locationSuccessResponse!=null) {
        print(response.locationSuccessResponse!.timestamp);
        print(response.locationSuccessResponse!.heading);
      }else {
        print(response);
      }
    });
  }

}