import 'package:driver_share/app/data/remote_db/models/location_models/position_model.dart';
import 'package:get/get.dart';
import '../../../data/remote_db/provider/location_provider.dart';
import '../../../values/utilities/storage_pref.dart';
import 'location_controller.dart';

class GlobalComponentsController extends GetxController {
  final LocationController _locationController = Get.find<LocationController>();

  var storage = Get.put(StoragePref());
  late LocationProvider locationProvider;

  var currentPosition = Position().obs;

  @override
  void onInit() {
    locationProvider = LocationProvider();
    super.onInit();

    ever(_locationController.currentPosition, locationProcess);
  }

  locationProcess(Position position) {
    currentPosition(position);
    /*if (storage.isLoggedIn.val && storage.isDriverLoggedIn.val) {
      print("Position from foreground==> ${position.latitude}, ${position.longitude}");
      // Position position1 = await _determinePosition();
      // print(position1.latitude);
      // print(position1.longitude);
      // print(storage.sessionId.val);
      // print(storage.driverUuid.val);
    }*/
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
}
