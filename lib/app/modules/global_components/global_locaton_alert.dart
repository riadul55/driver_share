import 'package:flutter/material.dart';
import 'package:get/get.dart';

class GlobalLocationAlert {

  showAlertDialog() {

    // set up the button
    Widget okButton = TextButton(
      child: Text("OK"),
      onPressed: () { Get.back(); },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: const Text("Location Permission"),
      content: const Text("For better experience turn on the location permission."),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    Get.dialog(alert, barrierDismissible: true);
  }
}