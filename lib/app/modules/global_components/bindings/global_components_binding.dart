import 'package:driver_share/app/modules/global_components/controllers/location_controller.dart';
import 'package:get/get.dart';

import '../controllers/global_components_controller.dart';
import '../controllers/loading_controller.dart';

class GlobalComponentsBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(LocationController());
    Get.put(GlobalComponentsController());
    Get.put(LoadingController());
  }
}
