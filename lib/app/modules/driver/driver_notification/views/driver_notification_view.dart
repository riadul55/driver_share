import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../values/themes/app_themes.dart';
import '../controllers/driver_notification_controller.dart';

class DriverNotificationView extends GetView<DriverNotificationController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration:  const BoxDecoration(
          gradient: LinearGradient(
              colors: [onPrimary, Colors.red],
              begin: Alignment.topRight,
              end: Alignment.bottomLeft),
        ),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(0, 60, 0, 50),
          child: Column(
            children: [
              const Padding(
                padding: EdgeInsets.only(top: 60),
                child: Text(
                  "Calling...",
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.white, fontSize: 36),
                ),
              ),
              /*const Padding(
                padding: EdgeInsets.only(top: 10),
                child: CircleAvatar(
                  radius: 70,
                  backgroundImage: NetworkImage(
                      'https://cdn.wisden.com/wp-content/uploads/2017/11/GettyImages-688893396-e1520011475170.jpg'),
                ),
              ),*/
              const Padding(
                padding: EdgeInsets.only(top: 60),
                child: Text(
                  "Details here (if any)",
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
              ),

              /*Padding(
              padding: const EdgeInsets.all(50),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                Column(
                  children: const [
                    Icon(Icons.add,color: Colors.grey,size: 40,),
                    Text("Add Call", style: TextStyle(color: Colors.grey),),
                  ],
                ),Column(
                  children: const [
                    Icon(Icons.videocam,color: Colors.grey,size: 40,),
                    Text("Video Call", style: TextStyle(color: Colors.grey),),
                  ],
                ),
                Column(
                  children: const [
                    Icon(Icons.bluetooth,color: Colors.grey,size: 40,),
                    Text("Bluetooth", style: TextStyle(color: Colors.grey),),
                  ],
                ),

              ],),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(50,0,50,0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                Column(
                  children: const [
                    Icon(Icons.volume_up,color: Colors.green,size: 40,),
                    Text("Speaker", style: TextStyle(color: Colors.grey),),
                  ],
                ),Column(
                  children: const [
                    Icon(Icons.mic_off,color: Colors.grey,size: 40,),
                    Text("Mute", style: TextStyle(color: Colors.grey),),
                  ],
                ),
                Column(
                  children: const [
                    Icon(Icons.dialpad,color: Colors.grey,size: 40,),
                    Text("Dialpad", style: TextStyle(color: Colors.grey),),
                  ],
                ),

              ],),
            ),*/
              Spacer(),
              Spacer(),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Spacer(),
                  InkWell(
                    onTap: () {},
                    child: Container(
                      child: const Padding(
                        padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                        child: Center(
                            child: Text(
                              "Decline",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 28,
                                  fontWeight: FontWeight.bold),
                            )),
                      ),
                      decoration: const BoxDecoration(
                          color: Colors.red,
                          borderRadius: BorderRadius.all(Radius.circular(5))),
                    ),
                  ),
                  Spacer(),
                  InkWell(
                    onTap: () {},
                    child: Container(
                      child: const Padding(
                        padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                        child: Center(
                            child: Text(
                              "Accept",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 28,
                                  fontWeight: FontWeight.bold),
                            )),
                      ),
                      decoration: const BoxDecoration(
                          color: Colors.green,
                          borderRadius: BorderRadius.all(Radius.circular(5))),
                    ),
                  ),
                  Spacer(),
                ],
              ),
              Expanded(
                child: Container(),
              )
            ],
          ),
        ),
      ),
    );
  }
}
