import 'package:get/get.dart';

import '../controllers/driver_setting_controller.dart';

class DriverSettingBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DriverSettingController>(
      () => DriverSettingController(),
    );
  }
}
