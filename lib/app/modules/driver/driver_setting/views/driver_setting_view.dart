import 'package:driver_share/app/modules/driver/driver_setting/views/screen/driver_setting_mobile_body.dart';
import 'package:driver_share/app/modules/driver/driver_setting/views/screen/driver_setting_tab_body.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controllers/driver_setting_controller.dart';


class DriverSettingView extends GetView<DriverSettingController> {
  const DriverSettingView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery
        .of(context)
        .size;
    if (size.width > 480 && size.width <= 1280) {
      //For Tab
      return DriverSettingTabBody(controller: controller);
    }else {
      //For Mobile
      return DriverSettingMobileBody(controller: controller);
    }
  }
}
