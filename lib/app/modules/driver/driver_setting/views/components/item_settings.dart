import 'package:driver_share/app/values/utilities/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ItemSettings extends StatelessWidget {

  final String title;
  final String? subTitle;
  final Function()? press;

  const ItemSettings(
      {Key? key,  this.press, required this.title, this.subTitle})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      color: Colors.white70,
      child: ListTile(
        title: Text(title,style: const TextStyle(fontSize: 18),),
        subtitle: subTitle !=null ? Text(subTitle!,style: const TextStyle(fontSize: 14),) :null,
        onTap: press,
      ),
    );
  }
}
