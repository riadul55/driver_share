import 'package:driver_share/app/modules/global_components/base_widget.dart';
import 'package:flutter/material.dart';
import 'package:driver_share/app/modules/driver/driver_setting/controllers/driver_setting_controller.dart';
import 'package:get/get.dart';

import '../../../../../values/utilities/constants.dart';

class DriverSettingTabBody extends StatelessWidget {
  final DriverSettingController controller;

  const DriverSettingTabBody({Key? key, required this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseWidget(child: Scaffold(
      appBar: AppBar(
        title:  Text("Setting",
          style: TextStyle(
            color: Theme.of(context).colorScheme.onPrimary,
            fontWeight: FontWeight.bold),),

      ),
      body: Container(
        child: const Center(child: Text("Tab Setting"),),
      ),
    ));
  }
}
