import 'package:driver_share/app/modules/driver/driver_setting/controllers/driver_setting_controller.dart';
import 'package:driver_share/app/modules/driver/driver_setting/views/components/item_settings.dart';
import 'package:driver_share/app/modules/global_components/base_widget.dart';
import 'package:flutter/material.dart';

import '../../../../../values/utilities/constants.dart';

class DriverSettingMobileBody extends StatelessWidget {
  final DriverSettingController controller;

  const DriverSettingMobileBody({Key? key, required this.controller})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseWidget(
        child: Scaffold(
            appBar: AppBar(
              elevation: 1,
              title: Text(
                "Setting",
                style: TextStyle(
                    color: Theme.of(context).colorScheme.onPrimary,
                    fontWeight: FontWeight.bold),
              ),
            ),
            body: SingleChildScrollView(
              child: Column(
                children: const [
                  SizedBox(
                    height: 20,
                  ),
                  ItemSettings(
                    title: "Sync to calendar",
                  ),
                  ItemSettings(
                    title: "Theme",
                    subTitle: "Light",
                  ),
                  ItemSettings(
                    title: "Change Password",
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  ItemSettings(
                    title: "Acknowledgment",
                  ),
                  // ItemSettings(icon: Icons.dark_mode,itemName: "Theme",),
                ],
              ),
            )));
  }
}
