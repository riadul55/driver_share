import 'package:driver_share/app/modules/driver/driver_trip/views/screen/driver_trip_mobile_body.dart';
import 'package:driver_share/app/modules/driver/driver_trip/views/screen/driver_trip_tab_body.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/driver_trip_controller.dart';

class DriverTripView extends GetView<DriverTripController> {
  const DriverTripView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    if(size.width>480 && size.width<=1280){
      // For Tab
      return DriverTripTabBody(controller: controller);
    }else{
      // For Mobile
      return  DriverTripMobileBody(controller: controller);
    }
  }
}
