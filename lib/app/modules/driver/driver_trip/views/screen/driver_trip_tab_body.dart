import 'package:driver_share/app/modules/driver/driver_trip/controllers/driver_trip_controller.dart';
import 'package:flutter/material.dart';

import '../../../../global_components/base_widget.dart';
import '../../../driver_home/views/components/item_order_list_tab.dart';

class DriverTripTabBody extends StatelessWidget {
  final DriverTripController controller;

  const DriverTripTabBody({Key? key, required this.controller})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseWidget(
        child: Scaffold(
      appBar: AppBar(
        elevation: 1,
        title: Text(
          "Trip Status",
          style: TextStyle(
              color: Theme.of(context).colorScheme.onPrimary,
              fontWeight: FontWeight.bold),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            ItemOrderLIstTab(
              orderNo: "A2VB67D",
              customerName: "Imran Hossain",
              arrivalTime: "10:30 AM",
            ),
            ItemOrderLIstTab(
              orderNo: "B54FGY8",
              customerName: "Riadul Islam",
              arrivalTime: "08:40 PM",
            ),
            const SizedBox(
              height: 30,
            ),
            const SizedBox(
              height: 60,
            ),

            // ElevatedButton(
            //   onPressed: () {
            //     AwesomeNotifications().dismiss(controller.uuId);
            //   },
            //   child:  Text("Notification",style: TextStyle(
            //       fontSize: 18,
            //       color: Theme.of(context).colorScheme.surface,
            //       fontWeight: FontWeight.bold),
            //     maxLines: 1,
            //     overflow: TextOverflow.ellipsis,),
            // ),
          ],
        ),
      ),
    ));
  }
}
