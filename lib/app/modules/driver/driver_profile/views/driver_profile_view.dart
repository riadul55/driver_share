import 'package:driver_share/app/modules/driver/driver_profile/controllers/driver_profile_controller.dart';
import 'package:driver_share/app/modules/driver/driver_profile/views/screen/driver_profile_mobile_body.dart';
import 'package:driver_share/app/modules/driver/driver_profile/views/screen/driver_profile_tab_body.dart';
import 'package:driver_share/app/modules/global_components/base_widget.dart';
import 'package:driver_share/app/values/utilities/constants.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../global_components/views/global_bottom_sheet_container.dart';
import 'components/bottom_sheet_edit_information.dart';

class DriverProfileView extends GetView<DriverProfileController> {

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery
        .of(context)
        .size;
    if (size.width > 480 && size.width <= 1280) {
      //For Tab
      return DriverProfileTabBody(controller: controller);
    }else {
      //For Mobile
      return DriverProfileMobileBody(controller: controller);
    }
  }


}
