import 'package:driver_share/app/modules/driver/driver_profile/controllers/driver_profile_controller.dart';
import 'package:driver_share/app/modules/driver/driver_profile/views/components/bottom_sheet_edit_information.dart';
import 'package:driver_share/app/modules/global_components/base_widget.dart';
import 'package:driver_share/app/values/themes/app_themes.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import '../../../../../values/utilities/constants.dart';
import '../../../../global_components/views/global_bottom_sheet_container.dart';
import '../components/bottom_sheet_edit_image.dart';


class DriverProfileMobileBody extends StatelessWidget {
  final DriverProfileController controller ;
  DriverProfileMobileBody({Key? key, required this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseWidget(
      child: Scaffold(
        appBar: AppBar(
          title: const Text("Profile",),
          actions: [
            IconButton(
              onPressed: () {editInformation(context);},
              icon: const Icon(Icons.edit),
            ),
            SizedBox(width: Constants.defaultPadding/2,)
          ],
        ),
        body: SingleChildScrollView(
          child:Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Center(
                child: GestureDetector(
                  onTap: () => _editProfilePic(context),
                  child: Container(
                    height: Constants.defaultPadding * 6,
                    width: Constants.defaultPadding * 6,
                    margin: EdgeInsets.only(top: Constants.defaultPadding),
                    child: Stack(
                      children: [
                        const CircleAvatar(
                          radius: 72,
                          backgroundImage: NetworkImage(
                              'https://cdn.wisden.com/wp-content/uploads/2017/11/GettyImages-688893396-e1520011475170.jpg'),
                        ),
                        // cachedProfileImage(
                        //   imageUrl: userResponse != null &&
                        //       userResponse.user != null &&
                        //       userResponse.user!.photo != null
                        //       ? userResponse.user!.photo
                        //       : "",
                        //   name: userResponse != null &&
                        //       userResponse.user != null &&
                        //       userResponse.user!.name != null ? userResponse
                        //       .user!.name : "E",
                        // ),
                        Align(
                          alignment: Alignment.bottomRight,
                          child: Container(
                            height: 30,
                            width: 30,
                            margin: EdgeInsets.only(right: 10),
                            decoration: BoxDecoration(
                              color:
                              Theme
                                  .of(context)
                                  .colorScheme
                                  .primaryVariant,
                              shape: BoxShape.circle,
                            ),
                            child: const Icon(
                              Icons.edit,
                              color: Colors.white,
                              size: 18,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: Constants.defaultPadding * 2,
              ),
              Padding(
                padding: EdgeInsets.only(
                  left: Constants.defaultPadding,
                  right: Constants.defaultPadding,
                ),
                child: Text(
                  "Personal Information",
                  style: TextStyle(
                    color: Theme
                        .of(context)
                        .colorScheme
                        .onBackground,
                    fontSize: Constants.sText,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ),
              const ItemProfile(
                icon: FontAwesomeIcons.userLarge,
                title: "Name",
                subTitle: "Imran Hossain",
                /*subTitle: userResponse != null &&
                        userResponse.user != null &&
                        userResponse.user!.name != null
                        ? "${userResponse.user!.name}"
                        : "",*/
              ),const ItemProfile(
                icon: Icons.email,
                title: "E-mail",
                subTitle: "imran@gmail.com",
                /*subTitle: _controller.email.value.isNotEmpty
                        ? "${_controller.email.value}"
                        : "not_provided".tr,*/
              ),
              const ItemProfile(
                icon: FontAwesomeIcons.phone,
                title: "Contact Number",
                subTitle: "01776482727",
                /*subTitle: userResponse != null &&
                        userResponse.user != null &&
                        userResponse.user!.phone != null
                        ? "${userResponse.user!.phone}"
                        : "",*/
              ),

              const ItemProfile(
                icon: FontAwesomeIcons.calendarDay,
                title: "Date Of Birth",
                subTitle: "Not Provider",
                /*subTitle: _controller.birthday.value.isNotEmpty
                        ? "${_controller.birthday.value}"
                        : "not_provided".tr,*/
              ),
              const ItemProfile(
                icon: FontAwesomeIcons.transgender,
                title: "Gender",

                subTitle: "Male",
                /*subTitle: _controller.gender.value.isNotEmpty
                        ? "${_controller.gender.value}"
                        : "not_provided".tr,*/
              ),
            ],
          ),
        ),
      ),
    );
  }


  void editInformation(BuildContext context)  {
    globalBottomSheetContainer(
      context: context,
      child: BottomSheetEditInformation(controller: controller,),
    );
  }
  void _editProfilePic(BuildContext context) async {
    await globalBottomSheetContainer(
      context: context,
      // child: BottomSheetGetImage(),
      child: BottomSheetEditImage(controller: controller,),
    );
  }
}

class ItemProfile extends StatelessWidget {
  final IconData icon;
  final String title;
  final String subTitle;
  final Function()? press;

  const ItemProfile({Key? key,
    required this.icon,
    required this.title,
    required this.subTitle,
    this.press})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ListTile(
          onTap: press,
          leading: FaIcon(
            icon,
            color: onPrimary,
          ),
          title: Text(
            title,
            style: TextStyle(
              color: Theme
                  .of(context)
                  .colorScheme
                  .onBackground,
              fontSize: Constants.sText,
              fontWeight: FontWeight.w700,
            ),
          ),
          subtitle: Text(
            subTitle,
            style: TextStyle(
              color: Theme
                  .of(context)
                  .colorScheme
                  .onBackground,
              fontSize: Constants.xsText,
              fontWeight: FontWeight.w700,
            ),
          ),
        ),
        const Divider(color: Colors.grey,
          height: 2,),
      ],
    );
  }
}
