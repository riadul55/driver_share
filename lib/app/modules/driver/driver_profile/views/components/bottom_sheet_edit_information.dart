import 'package:driver_share/app/modules/driver/driver_profile/controllers/driver_profile_controller.dart';
import 'package:driver_share/app/values/themes/app_themes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_obx_widget.dart';
import 'package:intl/intl.dart';
import 'package:google_fonts/google_fonts.dart';
import '../../../../../values/utilities/constants.dart';
import '../../../../global_components/views/dorp_down_list.dart';
import '../../../../global_components/views/global_text_field_widget.dart';

class BottomSheetEditInformation extends StatelessWidget {
  final DriverProfileController controller;

  BottomSheetEditInformation({Key? key, required this.controller})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    double paddingSize = size.width - 80;
    return SizedBox(
      width: size.width,
      child: SingleChildScrollView(
        child: Padding(
          padding:
              const EdgeInsets.only(top: 25, bottom: 5, left: 40, right: 40),
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Align(
                  alignment: Alignment.topCenter,
                  child: Text(
                    "Edit information",
                    style: TextStyle(
                        color: Theme.of(context).colorScheme.onBackground,
                        fontSize: Constants.sText,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                SizedBox(
                  height: Constants.defaultPadding / 2,
                ),
                Text(
                  "Name",
                  style: TextStyle(
                    color: Theme.of(context).colorScheme.onBackground,
                    fontSize: Constants.sText,
                  ),
                ),
                GlobalTextFieldWidget(
                  hintText: "Enter name",
                  contentPadding: 12,
                  onChanged: (String value) {},
                ),
                SizedBox(
                  height: Constants.defaultPadding / 2,
                ),
                Text(
                  "Contact Number",
                  style: TextStyle(
                    color: Theme.of(context).colorScheme.onBackground,
                    fontSize: Constants.sText,
                  ),
                ),
                GlobalTextFieldWidget(
                  hintText: "Enter number",
                  contentPadding: 12,
                  onChanged: (String value) {},
                ),
                SizedBox(
                  height: Constants.defaultPadding / 2,
                ),
                Text(
                  "Date Of Birth",
                  style: TextStyle(
                    color: Theme.of(context).colorScheme.onBackground,
                    fontSize: Constants.sText,
                  ),
                ),
                InkWell(
                  onTap: () {
                    showDatePicker(
                        context: context,
                        initialDate: DateTime.now(),
                        firstDate: DateTime(DateTime.now().year - 50),
                        lastDate: DateTime(DateTime.now().year + 10),
                        builder: (context, child) {
                          return Theme(
                            data: ThemeData.light().copyWith(
                              colorScheme: ColorScheme.fromSwatch(
                                primarySwatch: Colors.deepPurple,
                                primaryColorDark: Colors.deepPurple,
                                accentColor: Colors.deepPurple,
                              ),
                              dialogBackgroundColor: Colors.white,
                            ),
                            child: child!,
                          );
                        }).then((dateTime) {
                      controller.dobController.text =
                          "${dateTime!.year}-${dateTime.month}-${dateTime.day}";
                      controller.isDOBNotValid(false);
                    });
                  },
                  child: IgnorePointer(
                    child: GlobalTextFieldWidget(
                      contentPadding: 12,
                      controller: controller.dobController,
                      hintText: "Enter date of birth",
                      onChanged: (value) => controller.isDOBNotValid(false),
                    ),
                  ),
                ),
                SizedBox(
                  height: Constants.defaultPadding / 2,
                ),
                Text(
                  "Gender",
                  style: TextStyle(
                    color: Theme.of(context).colorScheme.onBackground,
                    fontSize: Constants.sText,
                  ),
                ),
                Obx(() => DropDownList(
                      hintText: "Select gender",
                      selectedValue: controller.genderInputValue.value,
                      isNotValid:
                          controller.isGenderNotValid.value ? onPrimary : null,
                      items: const [
                        'Male',
                        'Female',
                        'Other',
                      ],
                      onChange: (value) {
                        controller.genderInputValue(value);
                        controller.genderInput = value;
                        controller.isGenderNotValid(false);
                      },
                    )),
                const SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                  onPressed: () {
                    FocusScope.of(context).requestFocus(FocusNode());
                    Get.back();
                    // controller.updateProfile();
                  }, child: Container(
                  width: size.width,
                  padding: const EdgeInsets.all(16),
                  child: Center(
                    child: Text(
                      "Update",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: Constants.lText,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                ),
                const SizedBox(
                  height: 20,
                ),
              ]),
        ),
      ),
    );
  }
}
