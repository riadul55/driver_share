import 'package:driver_share/app/modules/driver/driver_profile/controllers/driver_profile_controller.dart';
import 'package:driver_share/app/values/themes/app_themes.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';

import '../../../../../values/utilities/constants.dart';

class BottomSheetEditImage extends StatelessWidget {

  final DriverProfileController controller ;
  BottomSheetEditImage({Key? key, required this.controller, }) : super(key: key);



  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: Constants.defaultPadding/2,),
        Align(
          alignment: Alignment.topCenter,
          child: Text(
            "Choose Image Option",
            style: TextStyle(
                color: Theme.of(context).colorScheme.onSecondary,
                fontSize: Constants.sText,
                fontWeight: FontWeight.bold),
          ),
        ),
        SizedBox(
          height: Constants.defaultPadding / 4,
        ),
        ListView(
          physics: const NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          children: [
            ListTile(
              leading: const FaIcon(FontAwesomeIcons.camera,color: onPrimary,),
              title: Text(
                "Camera",
                style: TextStyle(
                    color: Theme.of(context).colorScheme.onSecondary,
                    fontSize: Constants.mText,
                    fontWeight: FontWeight.bold),
              ),
              onTap: () {
                Get.back();

                   controller.getImage(ImageSource.camera);

              },
            ),
            ListTile(
              leading: const FaIcon(FontAwesomeIcons.image,color: onPrimary,),
              title: Text(
                "Gallery",
                style: TextStyle(
                    color: Theme.of(context).colorScheme.onSecondary,
                    fontSize: Constants.mText,
                    fontWeight: FontWeight.bold),
              ),
              onTap: () {
                Get.back();
                   controller.getImage(ImageSource.gallery,);
              },
            ),
          ],
        ),
        SizedBox(
          height: Constants.defaultPadding,
        ),
      ],
    );
  }
}
