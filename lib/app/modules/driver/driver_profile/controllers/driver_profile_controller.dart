import 'dart:io';
import 'package:driver_share/app/modules/global_components/snack_bar.util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
class DriverProfileController extends GetxController {
  //TODO: Implement DriverDriverProfileController

  var nameController = TextEditingController();
  var numberController = TextEditingController();
  var dobController = TextEditingController();
  var name="".obs;
  var genderInputValue = "".obs;
  var genderInput = "";

  var isEmailNotValid = false.obs;
  var isDOBNotValid = false.obs;
  var isGenderNotValid = false.obs;
  var selectedDate= DateTime.now().obs;
  var finalDate="".obs;



  //profile pic upload
  var selectedImagePath = "".obs;
  var selectedImageSize = "".obs;

  //crop code
  var cropImagePath = ''.obs;
  var cropImageSize = ''.obs;

  //compress code
  var compressImagePath = ''.obs;
  var compressImageSize = ''.obs;

  @override
  void onInit() {
    super.onInit();
  }


  chooseDate() async {
    DateTime? pickedDate = await showDatePicker(
      context: Get.context!,
      initialDate: selectedDate.value,
      firstDate: DateTime(2000),
      lastDate: DateTime(2100),
    );
    if(pickedDate!=null && pickedDate!=selectedDate.value){
      selectedDate.value=pickedDate;
      finalDate.value=DateFormat("dd-MM-yyyy").format(selectedDate.value).toString();
    }
  }


  void getImage(ImageSource imageSource) async {
    final pickedFile = await ImagePicker().pickImage(source: imageSource);
    if (pickedFile != null) {
      selectedImagePath.value = pickedFile.path;
      selectedImageSize.value =
          ((File(selectedImagePath.value)).lengthSync() / 1024 / 1024)
              .toStringAsFixed(2) +
              " Mb";

      //Crop
      final cropImageFile = await ImageCropper.cropImage(
        sourcePath: selectedImagePath.value,
        maxWidth: 512,
        maxHeight: 512,
        compressFormat: ImageCompressFormat.jpg,
        // aspectRatio: CropAspectRatio(ratioX: 1.0, ratioY: 1.0),
      );
      cropImagePath.value = cropImageFile!.path;
      cropImageSize.value = ((File(cropImagePath.value)).lengthSync() / 1024 / 1024).toStringAsFixed(2) + " Mb";

      // Compress
      final dir = await Directory.systemTemp;
      final targetPath = dir.absolute.path + "/temp.jpg";
      var compressedFile = await FlutterImageCompress.compressAndGetFile(
          cropImagePath.value,
          targetPath, quality: 90);
      compressImagePath.value = compressedFile!.path;
      compressImageSize.value =
          ((File(compressImagePath.value)).lengthSync() / 1024 / 1024)
              .toStringAsFixed(2) +
              " Mb";

      // uploadImage(compressedFile);
    } else {
      SnackbarUtil.showError(message: "No image selected");
    }
  }

 /* void uploadImage(File compressedFile) {
    if (!_mainController.isConnected.value) {
      showErrorToast(
        title: "Network Error",
        msg: "Failed to get network connection!",
      );
    } else {
      Get.dialog(Center(child: CircularProgressIndicator()),
          barrierDismissible: false);
      //upload
      _authenticationProvider.updateProfilePic(compressedFile, userToken['token'])
          .then((AuthResponse response) {
        Get.back();
        if (response.status == "1") {
          getCustomerInfo.user!.email = emailInputController.text;
          getCustomerInfo.user!.birthday = dobInputController.text;
          getCustomerInfo.user!.gender = genderInputValue.value;
          _storage.write(GetStorageKey.USER_INFO, getCustomerInfo);
          _profileController.customerInfoResponse(getCustomerInfo);
          email(emailInputController.text);
          birthday(dobInputController.text);
          gender(genderInputValue.value);
          showSuccessToast(msg: response.message);
          _profileController.getUserInfo();
        } else {
          showErrorToast(msg: response.message);
        }
      });
    }
  }*/

}
