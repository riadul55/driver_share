import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../routes/app_pages.dart';


class ItemOrderLIstTab extends StatelessWidget {
  final String? orderNo;
  final String? customerName;
  final String? arrivalTime;
  final bool? isAcceptActive;
  final bool? isDeliveryActive;
  final bool? isCloseActive;
  final Color? bgColor;
  final Function()? acceptPress;
  final Function()? deliveryPress;
  final Function()? closePress;

  ItemOrderLIstTab({
    Key? key,
    this.orderNo,
    this.customerName,
    this.arrivalTime,
    this.isAcceptActive,
    this.isDeliveryActive,
    this.isCloseActive,
    this.bgColor,
    this.acceptPress,
    this.deliveryPress,
    this.closePress,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size=MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () {
        Get.toNamed(Routes.ORDER_DETAILS);
      },
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10,vertical:5),
        child: Container(
          padding: const EdgeInsets.all(8),
          width: size.width,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              //background color of box
              BoxShadow(
                color: bgColor ??
                    Theme.of(context).colorScheme.primaryContainer.withOpacity(0.3),
                blurRadius: 10.0, // soften the shadow
                spreadRadius: .5, //extend the shadow
                offset: const Offset(
                  8.0, // Move to right 10  horizontally
                  8.0, // Move to bottom 10 Vertically
                ),
              ),
            ],
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    SizedBox(
                      width: size.width*.33-20,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            const Text(
                              "Order Number",
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 14,
                              ),
                            ),
                            const SizedBox(height: 8,),
                            Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 5),
                              child: Text(
                                orderNo ?? "N/A",
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: const TextStyle(
                                  fontSize: 14,
                                ),
                              ),
                            ),
                            const SizedBox(height: 8,),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      width: size.width*.33-20,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            const Text(
                              "Customer Name",
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 14,
                              ),
                            ),
                            const SizedBox(height: 8,),
                            Text(
                              customerName ?? "N/A",
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: const TextStyle(
                                fontSize: 14,
                              ),
                            ),
                            const SizedBox(height: 8,),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      width: size.width*.33-20,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            const Text(
                              "Arrival Time",
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 14,
                              ),
                            ),
                            const SizedBox(height: 8,),
                            Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 5),
                              child: Text(
                                arrivalTime ?? "N/A",
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: const TextStyle(
                                  fontSize: 14,
                                ),
                              ),
                            ),
                            const SizedBox(height: 8,),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 50),
                    child: ElevatedButton(
                      onPressed: () {},
                      child:  Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 40),
                        child: Text("Accept",style: TextStyle(
                        fontSize: 16,
                        color: Theme.of(context).colorScheme.surface,
                      ),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 50),
                    child: ElevatedButton(
                      onPressed: () {},
                      child:  Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 40),
                        child: Text("Collecting",style: TextStyle(
                        fontSize: 16,
                        color: Theme.of(context).colorScheme.surface,
                      ),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 50),
                    child: ElevatedButton(
                      onPressed: () {},
                      child:  Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 40),
                        child: Text("Delivery",style: TextStyle(
                          fontSize: 16,
                          color: Theme.of(context).colorScheme.surface,
                        ),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 50),
                    child: ElevatedButton(
                      onPressed: () {},
                      child:  Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 40),
                        child:  Text("Close",style: TextStyle(
                          fontSize: 16,
                          color: Theme.of(context).colorScheme.surface,
                        ),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,),
                      ),
                    ),
                  ),
                ],),
              )
            ],
          ),
        ),
      ),
    );
  }
}
