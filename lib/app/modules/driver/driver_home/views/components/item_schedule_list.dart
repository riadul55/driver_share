import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class ItemScheduleLIst extends StatelessWidget {
  final String? title;
  final bool? isActive;
  final String? startTime;
  final String? endTime;
  final Color? bgColor;
  final Function()? press;

  ItemScheduleLIst(
      {Key? key,
      this.title,
      this.startTime,
      this.endTime,
      this.bgColor,
      this.press, this.isActive})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size=MediaQuery.of(context).size;
    return GestureDetector(
      // onTap: (){Get.toNamed(Routes.SCHEDULE);},
      onTap: (){},
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          width: size.width * .9,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              //background color of box
              BoxShadow(
                color: bgColor ??
                    Theme.of(context).colorScheme.primary.withOpacity(0.3),
                blurRadius: 10.0, // soften the shadow
                spreadRadius: .5, //extend the shadow
                offset: const Offset(
                  8.0, // Move to right 10  horizontally
                  8.0, // Move to bottom 10 Vertically
                ),
              ),
            ],
          ),
          child: Row(
            children: [
              Container(width: 10,color: Theme.of(context).colorScheme.primary,
              height: 60,),
              Padding(
                padding: EdgeInsets.all(10),
                child: Container(
                  width: size.width*.65,
                  child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(10),
                      child: Text(
                        title ?? "Schedule 1",
                        style: const TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 10,bottom: 10),
                      child: Row(
                        children: [Text(
                          startTime ?? "10:30 AM",
                          style: const TextStyle(
                            fontSize: 16,
                          ),
                        ),Text(
                          title ?? " to ",
                          style: const TextStyle(
                            fontSize: 16,
                          ),
                        ),Text(
                          endTime ?? "12:00 PM",
                          style: const TextStyle(
                            fontSize: 16,
                          ),
                        ),],
                      ),
                    )
                  ],
                ),),
              ),
              Container(width: size.width*.1,
                child: const Icon(Icons.arrow_circle_right_outlined,size: 28,color: Colors.cyanAccent,))
            ],
          ),
        ),
      ),
    );
  }
}
