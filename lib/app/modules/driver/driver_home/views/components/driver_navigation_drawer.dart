import 'package:driver_share/app/values/themes/app_themes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../routes/app_pages.dart';
import 'package:driver_share/app/modules/driver/driver_setting/views/driver_setting_view.dart';
import '../../../../../values/utilities/storage_pref.dart';
import '../../controllers/driver_home_controller.dart';


class DriverNavigationDrawer extends StatelessWidget {
  final DriverHomeController controller;
  DriverNavigationDrawer({Key? key, required this.controller}) : super(key: key);
  StoragePref storage = Get.find<StoragePref>();
  @override
  Widget build(BuildContext context) => Drawer(
    child: SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          buildHeader(context),
          buildMenuItems(context),
        ],
      ),
    ),
  );

  buildHeader(BuildContext context) => Container(
    color: Theme.of(context).colorScheme.primary,
    padding: EdgeInsets.only(
      top: MediaQuery.of(context).padding.top,
    ),
    child: Column(
      children: [
        const SizedBox(
          height: 8,
        ),
        const CircleAvatar(
          radius: 52,
          backgroundImage: NetworkImage(
              'https://cdn.wisden.com/wp-content/uploads/2017/11/GettyImages-688893396-e1520011475170.jpg'),
        ),
        const SizedBox(
          height: 12,
        ),
        Text(
          storage.loginID.val,
          style:  TextStyle(fontSize: 20, color: Theme.of(context).colorScheme.onPrimary),
        ),
        const SizedBox(
          height: 12,
        ),
      ],
    ),
  );

  buildMenuItems(BuildContext context) => Container(
    padding: const EdgeInsets.all(24),
    child: Wrap(
      runSpacing: 8,
      children: [
        ListTile(
          leading: Icon(Icons.view_list_outlined,
            color: Theme.of(context).colorScheme.primaryContainer),
          title:  Text("Trip Status", style:TextStyle(color: Theme.of(context).colorScheme.onPrimary),),
          onTap: () {
            //close navigation drawer before
            Navigator.pop(context);
            Get.toNamed(Routes.DRIVER_TRIP);
          },
        ),
        ListTile(
          leading:  Icon(Icons.person,
              color: Theme.of(context).colorScheme.primaryContainer),
          title:  Text("Profile", style:TextStyle(color: Theme.of(context).colorScheme.onPrimary),),
          onTap: () {
            //close navigation drawer before
            Navigator.pop(context);
            Get.toNamed(Routes.DRIVER_PROFILE);
          },
        ),
        const Divider(
          color: Colors.black54,
        ),
        ListTile(
          leading:  Icon(Icons.settings,
            color: Theme.of(context).colorScheme.primaryContainer,),
          title:  Text("Settings", style:TextStyle(color: Theme.of(context).colorScheme.onPrimary),),
          onTap: () {//close navigation drawer before
            Navigator.pop(context);
            Get.toNamed(Routes.DRIVER_SETTING);
            },
        ),
        ListTile(
          leading:  Icon(Icons.notifications,
              color: Theme.of(context).colorScheme.primaryContainer),
          title:  Text("Notification", style:TextStyle(color: Theme.of(context).colorScheme.onPrimary),),
          onTap: () {
            Navigator.pop(context);
            Get.toNamed(Routes.SCHEDULE);
          },
        ),
        ListTile(
          leading: Icon(Icons.logout_sharp,
              color: Theme.of(context).colorScheme.primaryContainer),
          title:  Text("Log out", style:TextStyle(color: Theme.of(context).colorScheme.onPrimary),),
          onTap: () {controller.signOut();},
        ),
      ],
    ),
  );
}
