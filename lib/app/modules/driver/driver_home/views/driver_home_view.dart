
import 'package:driver_share/app/modules/driver/driver_home/views/screen/driver_home_body.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../routes/app_pages.dart';
import '../../../global_components/base_widget.dart';
import '../controllers/driver_home_controller.dart';
import 'components/driver_navigation_drawer.dart';

class HomeView extends GetView<DriverHomeController> {
  var scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return BaseWidget(
      child: Scaffold(
        key: scaffoldKey,
        appBar: null,
        drawer: DriverNavigationDrawer(controller: controller,),
        body: DriverHomeBody(controller: controller,),
        floatingActionButtonLocation:
        FloatingActionButtonLocation.startTop,
        floatingActionButton: Padding(
          padding: const EdgeInsets.all(8.0),
          child: FloatingActionButton(
            // isExtended: true,
            child:  Icon(
              Icons.menu,
              color: Theme.of(context).colorScheme.surface,
            ),
            backgroundColor: Theme.of(context).colorScheme.primaryContainer,
            onPressed: () {
              scaffoldKey.currentState!.openDrawer();
            },
          ),
        ),
      ),
    );
  }
}

