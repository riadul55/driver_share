
import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:driver_share/app/modules/driver/driver_home/views/components/item_order_list_tab.dart';
import 'package:driver_share/app/services/awesome_notification.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';

import '../../controllers/driver_home_controller.dart';
import '../components/item_order_list.dart';
import '../components/item_schedule_list.dart';

class DriverHomeBody extends StatelessWidget {
  final DriverHomeController controller;
  DriverHomeBody({Key? key, required this.controller}) : super(key: key);
  late GoogleMapController googleMapController;
  CameraPosition initialPosition = const CameraPosition(
    zoom: 10,
    tilt: 0,
    bearing: 0,
    target: LatLng(52.2087498, -1.4616598),
  );
  @override
  Widget build(BuildContext context) {
    return Obx(() => GoogleMap(
      // polylines: controller.polylines,
      mapType: MapType.normal,
      markers: Set<Marker>.of(controller.marker),
      onMapCreated: controller.onMapCreated,
      // onMapCreated: (GoogleMapController ctr){
      //googleMapController=ctr;
      // },
      initialCameraPosition: initialPosition,
    ));
  }

}
