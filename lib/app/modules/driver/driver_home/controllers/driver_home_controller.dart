import 'dart:async';
import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:driver_share/app/data/remote_db/provider/location_provider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:socket_io_client/socket_io_client.dart';
import '../../../../data/remote_db/models/location_models/position_model.dart';
import '../../../../routes/app_pages.dart';
import '../../../../services/awesome_notification.dart';
import '../../../../values/utilities/storage_pref.dart';
import '../../../global_components/controllers/global_components_controller.dart';
import '../../../global_components/controllers/loading_controller.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;

import '../../../global_components/controllers/location_controller.dart';
import '../../../global_components/log_info.dart';

class DriverHomeController extends GetxController {
  final _loadingController = Get.find<LoadingController>();
  final globalController = Get.find<GlobalComponentsController>();
  late GoogleMapController googleMapController;
  final Completer<GoogleMapController> _completerController = Completer();
  final LocationController locationController = Get.find<LocationController>();
  late final LocationProvider _locationProvider;
  StoragePref storage = Get.find<StoragePref>();

  var currentPosition = Position().obs;
  var currentLat = 0.0.obs;
  var currentLng = 0.0.obs;
  Timer? timer;
  var index = 0.obs;
  var selectedDate = DateTime.now().obs;

  List<Marker> marker = <Marker>[].obs;
  var lat = "".obs;
  var lon = "".obs;

  late IO.Socket socket;

  @override
  void onInit() {
    super.onInit();
    ever(locationController.currentPosition, locationProcess);
    timer = Timer.periodic(
        const Duration(seconds: 5), (Timer t) => updatePosition());

    AwesomeNotifications().isNotificationAllowed().then((isAllowed) {
      if(!isAllowed){
        showAlertDialog();
      }
    });
    _locationProvider = LocationProvider();
    socketConnection();
    AwesomeNotifications().createdStream.listen((notification) { 
      ///when get a driver_notification worked at the same time
    });
    
    
    AwesomeNotifications().actionStream.listen((notification) {
      if(notification.channelKey=='call_channel'){
        if(notification.buttonKeyPressed=="Accept"){
          print("Accept key pressed");
          dynamic emitData = {
            "uuid" : notification.payload!["uuid"],
            "trip_uuid" : notification.payload!["trip_uuid"],
            "driver_uuid" : storage.driverUuid.val,
          };
          socket.emit("ACCEPTED", emitData);
          print(notification.payload.toString());
        } else if(notification.buttonKeyPressed=="Decline"){
          print("Decline key pressed");
          dynamic emitData = {
            "trip_uuid" : notification.payload!["trip_uuid"],
            "driver_uuid" : storage.driverUuid.val,
          };
          socket.emit("DECLINED", emitData);
        }
      }


      // else{
      //   Get.toNamed(Routes.DRIVER_NOTIFICATION);
      // }
    });
    
    
  }

  showAlertDialog() {
    // set up the button
    Widget okButton = TextButton(
      child: const Text("Allow"),
      onPressed: () {
        AwesomeNotifications().requestPermissionToSendNotifications().then((_) =>Get.back());
      },
    );
    Widget cancelButton = TextButton(
      child: const Text("Don't Allow"),
      onPressed: () { Get.back(); },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: const Text("Allow Notification"),
      content: const Text("Out app would like to send you notifications"),
      actions: [
        okButton,
        cancelButton
      ],
    );

    // show the dialog
    Get.dialog(alert, barrierDismissible: true);
  }

  void onMapCreated(GoogleMapController controller) {
    _completerController.complete(controller);
    logInfo("map creating....");
    updatePosition();
  }

  void updatePosition() async {
    logInfo("Driver position is updating.....");
    var sourcePosition = LatLng(currentLat.value, currentLng.value);
    final GoogleMapController ctr = await _completerController.future;

    CameraPosition cameraPosition = CameraPosition(
        zoom: 15,
        tilt: 0,
        bearing: 0,
        target: sourcePosition);
    ctr.animateCamera(CameraUpdate.newCameraPosition(cameraPosition));
    marker.removeWhere((item) => item.markerId.value == storage.providerUuid.val);

    marker.add(Marker(
        markerId: MarkerId(storage.driverUuid.val),
        position: sourcePosition,
        infoWindow: InfoWindow(
          title: storage.providerUuid.val,
          // snippet: "${place[0].street}, "
          //     "${place[0].subLocality}, "
          //     "${place[0].locality}"
        )));

    update();
  }

  locationProcess(Position position) {
    currentPosition(position);
    currentLat.value = position.latitude!;
    currentLng.value = position.longitude!;

    print("Position  foreground==> ${position.latitude}, "
        "${position.longitude}");
  }






  void signOut() {
    storage.isLoggedIn.val = false;
    storage.isDriverLoggedIn.val = false;
    storage.sessionId.val = "";
    storage.driverUuid.val = "";
    storage.providerUuid.val = "";
    storage.loginID.val="";

    Get.offAllNamed(Routes.LOGIN);
  }




  void socketConnection() {
    logInfo("connecting socket....");
    socket = IO.io('http://192.168.0.108:4000', <String, dynamic>{
      'transports': ['websocket'],
      'autoConnect': false,
    });
    socket.connect();
    socket.onConnect((_) {
      logSuccess('socket is connected!');
    });

    socket.on('ORDER_NOTIFICATION_${storage.driverUuid.val}', (data) {
      logInfo(data);
      if (data != null && data["trip_uuid"] != null) {
        Map<String, String> mapData = {
          "trip_uuid" : "${data["trip_uuid"]}",
          "message" : "${data["message"]}",
        };
        createNotificationWithActionButton(data: mapData, uId: data["uuid"]);
      }
    });

    socket.on('CLOSE_NOTIFICATION', (data) {
      logInfo(data);
      AwesomeNotifications().dismiss(int.parse(data["uuid"]));
    });

    socket.onDisconnect((_) {
      logError("socket is disconnected!");
    });
  }

  @override
  void onClose() {
    if (timer != null) {
      timer!.cancel();
    }
    super.onClose();
  }

}
