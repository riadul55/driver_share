import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../global_components/controllers/loading_controller.dart';
import '../../global_components/snack_bar.util.dart';

class ScheduleController extends GetxController {
  // final AuthRepository _authRepository;
  final _loadingController = Get.find<LoadingController>();
  //
  // ScheduleController({required AuthRepository authRepository})
  //     : _authRepository = authRepository;

  var selectedTime=TimeOfDay.now().obs;
  var selectedDate= DateTime.now().obs;

  chooseDate() async {
    DateTime? pickedDate = await showDatePicker(
      context: Get.context!,
      initialDate: selectedDate.value,
      firstDate: DateTime(2000),
      lastDate: DateTime(2100),
    );
    if(pickedDate!=null && pickedDate!=selectedDate.value){
      selectedDate.value=pickedDate;
    }
  }
  choseNextDate() async{
    // if(selectedDate.value.compareTo(DateTime.now())<0){
    if(selectedDate.value.day < DateTime.now().day){

      selectedDate.value=selectedDate.value.add(const Duration(days: 1));
    }else{
      SnackbarUtil.showWarning(message: "Future date can not applicable!");
    }
  }
  chosePreviousDate() async{

    selectedDate.value=selectedDate.value.subtract(const Duration(days: 1));

  }

  chooseTime() async {
    TimeOfDay ? pickedTime = await showTimePicker(
        context: Get.context!,
        initialTime: selectedTime.value,
        helpText: "Select Start Time",
        hourLabelText: 'Select Hour',
        minuteLabelText: 'Select Minute',
        errorInvalidText: 'Provide valid time'
    );
    print("Time:=====> "+pickedTime.toString());
    if (pickedTime != null && pickedTime != selectedTime.value) {
      selectedTime.value = pickedTime;
    }
  }

}
