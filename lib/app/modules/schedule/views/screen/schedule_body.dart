
import 'package:driver_share/app/modules/schedule/controllers/schedule_controller.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../../../driver/driver_home/views/components/item_schedule_list.dart';



class ScheduleBody extends StatelessWidget {
  final ScheduleController controller;
  const ScheduleBody({Key? key, required this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          const SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              IconButton(
                onPressed: () {controller.chosePreviousDate();},
                icon: const Icon(Icons.arrow_back_ios, size: 28),
              ),
              InkWell(
                onTap: () {
                  controller.chooseDate();
                },
                child: Padding(
                  padding: EdgeInsets.all(5),
                  child: Obx( () {
                    return Text(
                      DateFormat("dd-MM-yyyy").format(controller.selectedDate.value).toString(),style: TextStyle(fontSize: 20),
                    );
                  }
                  ),
                ),
              ),
              IconButton(
                disabledColor: Colors.grey,
                onPressed: (){controller.choseNextDate();},
                icon: const Icon(Icons.arrow_forward_ios, size: 28),
              )
            ],
          ),
          ItemScheduleLIst(),
          ItemScheduleLIst(),
          ItemScheduleLIst(),

          const SizedBox(
            height: 30,
          ),
          Obx(
                () => Text(
              "Time : ${controller.selectedTime.value.hour}:${controller.selectedTime.value.minute}",
              style: TextStyle(fontSize: 24),),
          ),

          const SizedBox(
            height: 30,
          ),
        ],
      ),
    );
  }
}
