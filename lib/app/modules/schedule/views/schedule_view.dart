import 'package:driver_share/app/modules/schedule/views/screen/schedule_body.dart';
import 'package:flutter/material.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_obx_widget.dart';
import 'package:get/get_state_manager/src/simple/get_view.dart';

import '../../global_components/base_widget.dart';
import '../controllers/schedule_controller.dart';

class ScheduleView extends GetView<ScheduleController> {
  ScheduleView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseWidget(
      child: Scaffold(
          appBar: AppBar(
            title: const Text('Schedule'),
            centerTitle: false,
            actions: [
              IconButton(
                onPressed: () {
                  controller.chooseTime();
                },
                icon: Icon(Icons.add_circle),
              ),
              SizedBox(
                width: 30,
              )
            ],
          ),
          body:  ScheduleBody(controller: controller)
          ),
    );
  }
}
