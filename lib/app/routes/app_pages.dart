import 'package:get/get.dart';

import '../modules/auth/bindings/auth_binding.dart';
import '../modules/auth/bindings/forgot_password_binding.dart';
import '../modules/auth/bindings/login_binding.dart';
import '../modules/auth/bindings/sign_up_binding.dart';
import '../modules/auth/views/auth_view.dart';
import '../modules/auth/views/screens/forgot_password_screen.dart';
import '../modules/auth/views/screens/login_screen.dart';
import '../modules/auth/views/screens/sign_up_screen.dart';
import '../modules/driver/driver_home/bindings/driver_home_binding.dart';
import '../modules/driver/driver_home/views/driver_home_view.dart';
import '../modules/driver/driver_notification/bindings/driver_notification_binding.dart';
import '../modules/driver/driver_notification/views/driver_notification_view.dart';
import '../modules/driver/driver_profile/bindings/driver_profile_binding.dart';
import '../modules/driver/driver_profile/views/driver_profile_view.dart';
import '../modules/driver/driver_setting/bindings/driver_setting_binding.dart';
import '../modules/driver/driver_setting/views/driver_setting_view.dart';
import '../modules/driver/driver_trip/bindings/driver_trip_binding.dart';
import '../modules/driver/driver_trip/views/driver_trip_view.dart';
import '../modules/global_components/bindings/global_components_binding.dart';
import '../modules/global_components/views/global_components_view.dart';
import '../modules/order_details/bindings/order_details_binding.dart';
import '../modules/order_details/views/order_details_view.dart';
import '../modules/provider/provider_home/bindings/provider_home_binding.dart';
import '../modules/provider/provider_home/views/provider_home_view.dart';
import '../modules/provider/provider_map/bindings/provider_map_binding.dart';
import '../modules/provider/provider_map/views/provider_map_view.dart';
import '../modules/provider/provider_setting/bindings/provider_setting_binding.dart';
import '../modules/provider/provider_setting/views/provider_setting_view.dart';
import '../modules/schedule/bindings/schedule_binding.dart';
import '../modules/schedule/views/schedule_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static final routes = [
    GetPage(
      name: _Paths.DRIVER_HOME,
      page: () => HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.AUTH,
      page: () => AuthView(),
      binding: AuthBinding(),
    ),
    GetPage(
      name: _Paths.LOGIN,
      page: () => LoginScreen(),
      binding: LoginBinding(),
    ),
    GetPage(
      name: _Paths.SIGNUP,
      page: () => SignUpScreen(),
      binding: SignUpBinding(),
    ),
    GetPage(
      name: _Paths.FORGOTPASSWORD,
      page: () => ForgotPasswordScreen(),
      binding: ForgotPasswordBinding(),
    ),
    GetPage(
      name: _Paths.DRIVER_SETTING,
      page: () => DriverSettingView(),
      binding: DriverSettingBinding(),
    ),
    GetPage(
      name: _Paths.SCHEDULE,
      page: () => ScheduleView(),
      binding: ScheduleBinding(),
    ),
    GetPage(
      name: _Paths.GLOBAL_COMPONENTS,
      page: () => GlobalComponentsView(),
      binding: GlobalComponentsBinding(),
    ),
    GetPage(
      name: _Paths.ORDER_DETAILS,
      page: () => OrderDetailsView(),
      binding: OrderDetailsBinding(),
    ),
    GetPage(
      name: _Paths.PROVIDER_HOME,
      page: () => ProviderHomeView(),
      binding: ProviderHomeBinding(),
    ),
    GetPage(
      name: _Paths.PROVIDER_SETTING,
      page: () => ProviderSettingView(),
      binding: ProviderSettingBinding(),
    ),
    GetPage(
      name: _Paths.PROVIDER_MAP,
      page: () => ProviderMapView(),
      binding: ProviderMapBinding(),
    ),
    GetPage(
      name: _Paths.DRIVER_PROFILE,
      page: () => DriverProfileView(),
      binding: DriverProfileBinding(),
    ),
    GetPage(
      name: _Paths.DRIVER_NOTIFICATION,
      page: () => DriverNotificationView(),
      binding: DriverNotificationBinding(),
    ),
    GetPage(
      name: _Paths.DRIVER_TRIP,
      page: () => DriverTripView(),
      binding: DriverTripBinding(),
    ),
  ];
}
