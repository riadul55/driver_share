part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();
  static const DRIVER_HOME = _Paths.DRIVER_HOME;
  static const PROVIDER_HOME = _Paths.PROVIDER_HOME;
  static const AUTH = _Paths.AUTH;
  static const LOGIN = _Paths.LOGIN;
  static const SIGNUP = _Paths.SIGNUP;
  static const FORGOTPASSWORD = _Paths.FORGOTPASSWORD;
  static const DRIVER_SETTING = _Paths.DRIVER_SETTING;
  static const SCHEDULE = _Paths.SCHEDULE;
  static const GLOBAL_COMPONENTS = _Paths.GLOBAL_COMPONENTS;
  static const ORDER_DETAILS = _Paths.ORDER_DETAILS;
  static const PROVIDER_SETTING = _Paths.PROVIDER_SETTING;
  static const PROVIDER_MAP = _Paths.PROVIDER_MAP;
  static const DRIVER_PROFILE = _Paths.DRIVER_PROFILE;
  static const DRIVER_NOTIFICATION = _Paths.DRIVER_NOTIFICATION;
  static const DRIVER_TRIP = _Paths.DRIVER_TRIP;
}

abstract class _Paths {
  _Paths._();
  static const DRIVER_HOME = '/driver_home';
  static const PROVIDER_HOME = '/provider_home';
  static const AUTH = '/auth';
  static const LOGIN = '/auth/login';
  static const SIGNUP = '/auth/signup';
  static const FORGOTPASSWORD = '/auth/forgot_pass';
  static const DRIVER_SETTING = '/driver_setting';
  static const SCHEDULE = '/schedule';
  static const GLOBAL_COMPONENTS = '/global-components';
  static const ORDER_DETAILS = '/order-details';
  static const PROVIDER_SETTING = '/provider_setting';
  static const PROVIDER_MAP = '/provider_map';
  static const DRIVER_PROFILE = '/driver-profile';
  static const DRIVER_NOTIFICATION = '/driver_notification';
  static const DRIVER_TRIP = '/driver_trip';
}
